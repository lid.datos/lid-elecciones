var cdds=new Array();
function Candidatura(sigla,vots) {
	this.sigla=sigla;
	this.vots=vots;
	this.elegits=0
}
function getInt(id) {
	num=jQuery(id).val().replace(/\.|,| |\-|\t|a-z/gi,"");
	num=(parseInt(num)!=num-0)?0:parseInt(num);
	if (parseInt(num)>0) {
		jQuery(id).val(num)
	}
	else {
		jQuery(id).val("")
	}
	return num
}
function calcular() {
	var i,esc,c,aux;
	var dividend=new Array();
	var tallDividend=-1;
	var divisor=new Array();
	numRep=getInt("#representants");
	minim=getInt("#minim");
	bl=getInt("#blanc");
	nu=getInt("#nul");
	totalVots=bl;
	for(i=0; i<cdds.length; i++) {
		dividend[i]=cdds[i].vots;
		divisor[i]=2;
		cdds[i].elegits=0;
		totalVots+=cdds[i].vots
	}
	var cens=getInt("#cens");
	minimVots=(minim*cens)/100;
	if (minimVots<=0) minimVots=1;
	bons=new Array();
	for(i=0; i<cdds.length; i++)if(cdds[i].vots<minimVots) {
		cdds[i].elegits=-1
	}
	else {
		bons.push(i)
	}
	if (numRep&&bons.length) {
		for (esc=0; esc<numRep; esc++)
			{
			candidata=bons[0];
			for (c=0; c<bons.length;	c++) {
				if(dividend[bons[c]]>dividend[candidata]) {
					candidata=bons[c]
				}
				else if (dividend[bons[c]]==dividend[candidata]) {
					if (cdds[bons[c]].vots>cdds[candidata].vots)	{
						candidata=bons[c]
					}
				}
			}
			cdds[candidata].elegits++;
			tallDividend=dividend[candidata];
			dividend[candidata]=cdds[candidata].vots/divisor[candidata];
			divisor[candidata]++
		}
		ultimCand=candidata;
		seguentCand=-1;
		var minDiffVots=-1;
		for(c=0; c<bons.length;	c++)
			if(bons[c]!=ultimCand) {
			var diffVots=tallDividend*(divisor[bons[c]]-1)-cdds[bons[c]].vots;
			if (seguentCand==-1||diffVots<minDiffVots) {
				seguentCand=bons[c];
				minDiffVots=diffVots
			}
		}
		votsMes=Math.floor(minDiffVots+1);
		ordrebo=new Array();
		for (i=0; i<cdds.length; i++) ordrebo.push(i);
		for (i=0; i<cdds.length; i++)
			for(j=0; j<cdds.length-1; j++)
			if(cdds[ordrebo[j+1]].vots>cdds[ordrebo[j]].vots) {
				aux=ordrebo[j];
				ordrebo[j]=ordrebo[j+1];
				ordrebo[j+1]=aux
			}
		}
		dibRes()
}
function updateSigla(pos,sigla) {
	if (pos<cdds.length) {
		cdds[pos].sigla=sigla;
		jQuery(".sig"+pos).html(cdds[pos].sigla)
	}
}
function updateVots(pos,vots) {
	if (pos<cdds.length) {
		cdds[pos].vots=(parseInt(vots)!=vots-0)?"":parseInt(vots);
		calcular()
	}
}
function elimCand(i) {
	cdds.splice(i,1);
	dibCands();
	calcular()
}
function addCand(i) {
	cdds.push(new Candidatura((jQuery("#sigla"+i))?jQuery("#sigla"+i).val():'',(jQuery("#vots"+i))?parseInt(jQuery("#vots"+i).val()):0));
	dibCands();
	calcular()
}