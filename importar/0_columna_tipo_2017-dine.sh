# Determina el tipo de dato en la base de acuerdo al nombre de la columna
case $1 in
*cod*prov*|*cod*distr*)
	echo $1 "VARCHAR(2) NOT NULL" ;;
*cod*dep*|*cod*municip*|*codmun*)
	echo $1 "VARCHAR(3) NOT NULL" ;;
*cod*mesa*|parcodigo|*cod*votos*|*cod*partido*) 
	echo $1 "VARCHAR(4) NOT NULL" ;;
*cod*circ*) 
	echo $1 "VARCHAR(5) NOT NULL" ;;
*porcen*)
	echo $1 "DECIMAL NOT NULL" ;;
*agrupacion_politica_tipo*) 
	echo $1 "VARCHAR(100)" ;;
*votos)
	echo $1 "INTEGER NOT NULL" ;;
*voto*|*total*|*electores*|,ind*)
	echo $1 "SMALLINT NOT NULL" ;;
# Menos frecuentes o que pueden venir vacíos (NULL)
*sexo*)
	echo $1 "VARCHAR(1)" ;;
*agrupacion) 
	echo $1 "VARCHAR(4)" ;;
*nombre*)
	echo $1 "VARCHAR(60)" ;;
*denominacion*|*lista_interna*)
	echo $1 "VARCHAR(100)" ;;
*)
	echo $1 "VARCHAR" ;;
esac