# Determina el tipo de dato en la base de acuerdo al nombre de la columna 
case $1 in
*coddis|*vot_procodigoprovincia|*mes_procodigoprovincia|*sub_procodigoprovincia| \
*depcodigoprovincia|*pre_procodigoprovincia|*sub_votprocodigoprovincia)
	echo $1 "VARCHAR(2) NOT NULL" ;;
*coddep|*vot_depcodigodepartamento|*vot_depcodigodepartamento|*sub_depcodigodepartamento| \
*pre_depcodigodepartamento|*sub_votdepcodigodepartamento|*vot_muncodigomunicipio|*mes_muncodigomunicipio| \
*sub_muncodigomunicipio|*pre_muncodigomunicipio|*sub_votmuncodigomunicipio)
	echo $1 "VARCHAR(3) NOT NULL" ;;
*vot_mescodigomesa|*mescodigomesa|*sub_mescodigomesa|*vot_parcodigo|*sub_votpar|*sub_votparcodigo| \
*ot_codigo|*ordenpartidos|*parcodigo) 
	echo $1 "VARCHAR(4) NOT NULL" ;;
*vot_mescodigocircuito|*mescodigocircuito|*sub_mescodigocircuito) 
	echo $1 "VARCHAR(5) NOT NULL" ;;
*porcen*|*preptotalvotantes*|*ot_pvotos*)
	echo $1 "DECIMAL NOT NULL" ;;
*pardenominacion) 
	echo $1 "VARCHAR(100)" ;;
*pretotalvotantes*|*ot_votos*|*prevotosenblanco*|*premesastotales*|*prevotosnulos*|*prevotosrecurridos*|*prevotosrecurridos*| \
*votvotospartido*|*subvotospartido*|*prevotosvalidos*|*prevotospositivos*)
	echo $1 "INTEGER NOT NULL" ;;
*votos*|*total*)
	echo $1 "SMALLINT NOT NULL" ;;
*ind*)
	echo $1 "SMALLINT NOT NULL" ;;
# Menos frecuentes o que pueden venir vacíos (NULL)
*sexo*)
	echo $1 "VARCHAR(1)" ;;
*parcolor) 
	echo $1 "VARCHAR(6) NOT NULL" ;;
*)
	echo $1 "VARCHAR" ;;
esac