Importar
========

En el repositorio mantenemos solamente los scripts.
Los scripts procesan datasets, generan tablas, archivos para importar y bases de datos que se guardan en el drive del proyecto.
O sea que para funcionar los scripts requieren que se descarguen los correspondientes datasets a procesar.


1 - Histórico de Resultados Electorales
---------------------------------------
Contiene los resultados electorales originales tal cual fueron publicados
https://drive.google.com/drive/folders/1WID99kekBzXNx8F6eNZX3pu7WIAkQaCR

2 - Datasets y tablas generadas
-------------------------------
Contiene los datasets preparados para los scripts y los directorios que generan dichos scripts.
Solamente son necesarios los datasets para el funcionamiento de los scripts, que se identifican con el año y la elección correspondiente, algunos están en desarrollo y todavía no se procesan.
Mientras que las tablas y archivos para importar, generados por los scripts tienen además un prefijo que las identifica y continene por ejemplo los archivos sql con la estructura de las tablas que sirven también a modo de documentación de las bases de datos.
https://drive.google.com/drive/folders/13pBut4wxTbIyXO-DOxCVJ74a2aVLjUrU

3 - Bases de Datos generadas
----------------------------
Contiene las bases de datos postgresql listas para importar directamente.
https://drive.google.com/drive/folders/1HBLBgYYzxbBlO_FfDg-FefyL-dP4Bdju

- **elecciones2019** Es la base de datos más completa que mencionamos antes. La generamos y usamos en 2019, mediante scripts pero de manera bastante manual.

- **elecciones2017** Subimos una nueva versión de base de datos de las elecciones 2017, que pueden descargarse e importar de la misma manera que la de elecciones 2019 ahora con las tablas geográficas: Metimos principalmente las dos grandes bases principales que estaban en Access del sitio de Andy que son las que vamos a trabajar. Y también metimos las de la dine que les pusimos el prefijo correspondiente para poder identificarlas.

- **elecciones2021-pronvinciales** Por ahora tenemos sólo Corrientes, luego subiermos el resto de las elecciones provinciales.

- **elecciones2021** Terminamos de armar una primer versión de base de datos 2021, con los resultados por mesa. 
  Mantenemos de antes:
  1) los resultados por sección que pasó Andy antes y las tablas derivadas que generamos
  2) las tablas geográficas de 2019
  3) las tablas que nos pasaron antes de las elecciones y sus tablas derivadas

