# Determina el tipo de dato en la base de acuerdo al nombre de la columna 
case $1 in
*mesa*|*gobernador*|*senadores*|*diputados*|*cantidad*)
	echo $1 "SMALLINT NOT NULL" ;;
*sobres*|*electores*|*votantes*)
	echo $1 "INTEGER NOT NULL" ;;
*intendente*|*concejales*|*convencionales*)
	echo $1 "SMALLINT" ;;
*partido|*nombre) 
	echo $1 "VARCHAR(100)" ;;
*rango|*estado) 
	echo $1 "VARCHAR(15)" ;;
*)
	echo $1 "VARCHAR" ;;
esac