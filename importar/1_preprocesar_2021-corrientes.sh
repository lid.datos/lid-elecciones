# Previamente se corre una primer normalización común a todas las bases
DIR_DESDE=$1
PREFIJO=$2
DIR_HASTA=$PREFIJO"_"$DIR_DESDE
ARCH_MESAS=$DIR_HASTA/$PREFIJO"_mesa_id_*_escrutinio_310821_1008sjr.csv"
PRIMERO=$DIR_HASTA/$PREFIJO"_mesa_id_1_escrutinio_310821_1008sjr.csv"
SALIDA=$DIR_HASTA/$PREFIJO"_mesa_escrutinio_310821_1008sjr.csv"
# Unifico archivos de mesas en uno sólo, agrego con número de mesa y agrego separadores que falten
echo "mesa,$(head -n 1 $PRIMERO),convencionales" > $SALIDA
echo "  "$(date)"- procesando mesas (aprox 15 min)" 
for i in $(ls $ARCH_MESAS); do
	# Elimino coma de un nombre que trae problemas
	sed -i 's/DE LA CULTURA,/DE LA CULTURA/g' $i
	MESA=$(basename "$i" | awk -F "_" '{print $4}')
	#echo -n " "$MESA
	sed 1d $i | while read line; do
		CANT=$(echo $line | grep -o -i "," | wc -l)
		if [[ $CANT = "4" ]]; then
			echo $MESA","$line",," >> $SALIDA
		elif [[ $CANT = "3" ]]; then
			echo $MESA","$line",,," >> $SALIDA
		elif [[ $CANT = "6" ]]; then
			echo $MESA","$line | sed 's/.$//' >> $SALIDA
		else
			echo $MESA","$line"," >> $SALIDA
		fi
	done
done
rm $ARCH_MESAS
echo "  "$(date)"- fin procesando mesas" 
# Esto es porque después en importar -> 1_renombrar_archivos -> procesa encoding distinto
# iconv -f UTF-8 -t ASCII $SALIDA > $SALIDA".temp"
# cp $SALIDA".temp" $SALIDA
# Reemplazo separadores y guiones
sed -i 's/\,/;/g' $SALIDA
sed -i 's/-/0/g' $SALIDA

# Muevo resto de archivos y agrego columna de rangos de mesa desde hasta
sed 's/\,/;/g' $DIR_HASTA/$PREFIJO"_mesas_departamento.csv" > $DIR_HASTA/"mesas_depto.csv"
awk -F ";" '{print ";"$2}' $DIR_HASTA/mesas_depto.csv | sed 's/"//g' | sed 's/ a /;/g' | sed 's/rango/mesa_desde;mesa_hasta/g' > $DIR_HASTA/mesas_depto_rango.csv
paste $DIR_HASTA/"mesas_depto.csv" $DIR_HASTA/"mesas_depto_rango.csv" > $DIR_HASTA/$PREFIJO"_mesas_departamento.csv"
rm $DIR_HASTA/"mesas_depto.csv" $DIR_HASTA/"mesas_depto_rango.csv"

sed 's/\,/;/g' $DIR_HASTA/$PREFIJO"_mesas_circuito.csv" > $DIR_HASTA/"mesas_circ.csv"
awk -F ";" '{print ";"$2}' $DIR_HASTA/mesas_circ.csv | sed 's/"//g' | sed 's/ a /;/g' | sed 's/rango/mesa_desde;mesa_hasta/g' > $DIR_HASTA/mesas_circ_rango.csv
paste $DIR_HASTA/"mesas_circ.csv" $DIR_HASTA/"mesas_circ_rango.csv" > $DIR_HASTA/$PREFIJO"_mesas_circuito.csv"
rm $DIR_HASTA/"mesas_circ.csv" $DIR_HASTA/"mesas_circ_rango.csv"