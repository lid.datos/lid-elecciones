#############################################################################################################
# importar.sh
#############################################################################################################
# ENTRADA
# Dada una serie de directorios con archivos csv separados por ; y un prefijo para cada uno
#############################################################################################################
# SALIDA
# Se genera otro directorio con los scripts de creación de tablas y los csv normalizados para importar
# Luego por cada archivo, se crea la tabla correspondiente con su prefijo y se importan los datos en la base
#############################################################################################################
# Debe estar instalado PostgreSQL y creada la base de datos. En este caso: CREATE DATABASE elecciones2017
# El script requiere tener instalado el comando dos2unix, que se instala con: sudo apt-get install dos2unix
#############################################################################################################
# Directorio por defecto de PostgreSQL
DIR_PG=/var/lib/postgresql
#############################################################################################################

function columna_tipo() {
	if [[ $1 == "2017-argentina"* ]]; then
		echo $(./0_columna_tipo_2017-argentina.sh $2)
	elif [[ $1 == *"dine"* ]]; then
		echo $(./0_columna_tipo_2017-dine.sh $2)
	elif [[ $1 == "2021-corrientes"* ]]; then
		echo $(./0_columna_tipo_2021-corrientes.sh $2)
	elif [[ $1 == "2021-argentina"* ]]; then
		echo $(./0_columna_tipo_2021-argentina.sh $2)
	fi
}

1_preprocesar_archivos() {
	echo "1_preprocesar: copiando archivos con nombres normalizados..." $1 $2
	./1_preprocesar.sh $1 $2
	if [[ $1 == "2021-corrientes"* ]]; then
		./1_preprocesar_2021-corrientes.sh $1 $2
	elif [[ $1 == "2021-argentina"* ]]; then
		./1_preprocesar_2021-argentina.sh $1 $2
	elif [[ $1 == "2021-generales"* ]]; then
		./1_preprocesar_2021-generales.sh $1 $2
	else
		DIR_HASTA=$2"_"$1
		for i in $(ls $DIR_HASTA/*.csv); do
			iconv -f ISO-8859-15 -t UTF-8 $i > $i.tmp
			# reemplazo caracteres especiales y muevo a directorio hasta
			sed -i 'y/áÁàÀãÃâÂéÉêÊíÍóÓõÕôÔúÚñÑçÇ/aAaAaAaAeEeEiIoOoOoOuUnNcC/' $i.tmp
			mv $i.tmp $i
		done
	fi
}

2_normalizar_columnas_header() {
	DIR=$2"_"$1
	echo "2_normalizar_columnas_header: normalizando headers para crear tablas... $DIR"
	# Lista archivos de DIR
	for i in $(ls $DIR/*.csv); do 
		# extrae primera fila de cada archivo
		head -1 $i > $i.1head
		# pasa a minúsculas y elimina espacios duplicados
		(tr [:upper:] [:lower:] | tr -s " ") < $i.1head > $i.2min
		# reemplazamos ";" por ",", eliminamos espacios antes/después, reemplazamos espacios por "_", eliminamos comillas
		sed -i 's/"//g'   $i.2min
		sed -i 's/;/\,/g' $i.2min
		sed -i -e 's/\, /\,/g' -e 's/ \,/\,/g' $i.2min
		sed -i 's/ /_/g'  $i.2min
		dos2unix -q $i.2min
		# convierto encoding (ver si no lo hacemos antes sobre los csv directamente)
		iconv -f ISO-8859-15 -t UTF-8 $i.2min > $i.sql
	done
	# Borro archivos temporales
    rm $DIR/*.1head $DIR/*.2min
}

3_header_a_sql() {
	echo "3_header_a_sql: genera los sql para crear tablas... $DIR"
	DIR=$2"_"$1
	for i in $(ls $DIR/*.sql); do 
		sed -i 's/\,/\n\,/g' $i
		# Creo tabla con el nombre del archivo sin extensión
 	    echo "CREATE TABLE IF NOT EXISTS" $(basename -s .csv.sql $i) "(" > $i"_2"
		while read line; do
			echo $(columna_tipo $1 $line) >> $i"_2"
		done < $i
 	    echo ");" >> $i"_2"
 	    echo "TRUNCATE $(basename -s .csv.sql $i);" >> $i"_2"
	done
	rm $DIR/*.sql
	# Renombro .csv.sql por .sql
	rename 's/.csv.sql_2/.sql/' $DIR/*
	# cat $DIR/*.sql
}

4_procesar_datos() {
	echo "4_procesar_datos: arreglos de archivos puntuales antes de importar..." $1 $2
	if [[ $1 == *"dine"* ]]; then
		./4_preprocesar_datos_2017_dine.sh $1 $2
	elif [[ $1 == *"2017-argentina"* ]]; then
		./4_preprocesar_datos_2017-argentina.sh $1 $2
	fi
}

5_crear_tablas() {
	DIR_DESDE=$2"_"$1
	DIR_HASTA=$DIR_PG/$DIR_DESDE
	echo "5_crear_tablas: copiando archivos sql para crear tablas y ejecutando en la base... $DIR_DESDE --> $DIR_HASTA"
	sudo cp -R $DIR_DESDE $DIR_HASTA
	sudo chmod 777 $DIR_HASTA
	for i in $(sudo ls $DIR_HASTA/*.sql); do
		#echo "creando tabla (si no existe)" $i
		sudo -u postgres -H -- psql -d $BASE_DE_DATOS -t -A -F";" -f "$i" -o "$i.txt"
	done
}

6_importar_datos() {
	DIR_DESDE=$2"_"$1
	DIR_HASTA=$DIR_PG/$DIR_DESDE
	echo "6_importar_datos: copiando archivos de datos csv e importando en la base... $DIR_DESDE --> $DIR_HASTA"
	for i in $(sudo ls $DIR_HASTA/*.csv); do
		tabla=$(basename -s .csv $i)
		#echo "importando datos de" $i "en tabla" $tabla
		sudo -u postgres -H -- psql -d $BASE_DE_DATOS -c "\COPY $tabla FROM '$i' DELIMITER ';' CSV HEADER;" -o "$i.txt"
	done
}

importar_tablas_geo() {
# Para importar tablas geográficas hay que instalar postgis en la base
	echo "7_importar_tablas_geo de la base de 2019 a la base" $BASE_DE_DATOS
	sudo -u postgres -H -- psql -d $1 $BASE_DE_DATOS -f postgis.sql
	./importar_tablas.sh p19_distritos $BASE_DE_DATOS
	./importar_tablas.sh p19_secciones $BASE_DE_DATOS
	./importar_tablas.sh p19_circuitos $BASE_DE_DATOS
	./importar_tablas.sh p19_ubicacion_lugares_votacion $BASE_DE_DATOS
}

borrar_directorios() {
	DIR_DESDE=$2"_"$1
	DIR_HASTA=$DIR_PG/$DIR_DESDE
	echo "borrando" $DIR_HASTA
	sudo rm -fR $DIR_HASTA
	echo "borrando" $DIR_DESDE
	rm -fR $DIR_DESDE
}

importar() {
################################################################################################
# Parámetros
################################################################################################
# DIR_ARCHIVOS: Donde están ubicados dichos archivos, csv separados por ";" con nombres de columnas no normalizados en la primera fila.
#          Por ejemplo: dine-geografico, dine-paso2017, dine-generales 
# PREFIJO: Prefijo para asignarles a cada grupo de tablas
#          Por ejemplo: p17 (paso 2017), g17 (geográficas 2017), e17 (elecciones generales 2017)
############################################################################################################
# 1 Generamos otro directorio con los nombres de archivos normalizados
# 2 Generamos en ese directorio los headers normalizados para sql
# 3 Generamos create table en sql para cada header de archivo normalizado
# 4 Procesamos archivos específicos que queden por ajustar para la importación
############################################################################################################
# 5 Creamos tablas si no existen (trabajamos en el directorio de postgres)
# 6 Importamos datos desde csv (trabajamos en el directorio de postgres)
# Borro directorios de trabajo temporales (se deben borrar o renombrar antes de volver a correr el script)
############################################################################################################
# - Se puede depurar el proceso corriendo cada paso uno por uno, 
#   dejando el resto y viendo los resultados que se generan.
# - Es necesario borrar los archivos temporales que se generan antes de cada corrida,
#   Por eso se intentan borrar antes si existen y opcionalmente se pueden borrar después.
# - Se ejecuta solicita realizar operaciones con sudo porque hay que operar en el directorio de postgres,
#   sobre el que tiene permisos el propio usuario de la base de datos.
# - Para borrar todas las tablas de un esquema en postgres desde un cliente postgres:
#   DROP SCHEMA public CASCADE;
#   CREATE SCHEMA public;
############################################################################################################
	borrar_directorios $1 $2
	1_preprocesar_archivos $1 $2
	#2_normalizar_columnas_header $1 $2
	#3_header_a_sql $1 $2
	#4_procesar_datos $1 $2
	#5_crear_tablas $1 $2
	#6_importar_datos $1 $2
	#borrar_directorios $1 $2
	echo
}

BASE_DE_DATOS=elecciones2017
echo
#importar 2017-dine-geografico g17
#importar 2017-dine-paso       p17
#importar 2017-dine-generales  e17
#importar 2017-argentina-paso  p17
#importar 2017-argentina       e17
#importar_tablas_geo

BASE_DE_DATOS=elecciones2021_provinciales
#importar 2021-corrientes c21
#importar_tablas_geo

BASE_DE_DATOS=elecciones2021
#importar 2021-argentina p21
#importar_tablas_geo
importar 2021-generales e21
