# DIR_DESDE: No debe tener subdirectorios y ni nombres de archivos con caracteres especiales como paréntesis o espacios
DIR_DESDE=$1
PREFIJO=$2
DIR_HASTA=$PREFIJO"_"$DIR_DESDE
# Copiar carpeta y borrar los que tienen una fila o menos (tablas vacías)
cp -r $DIR_DESDE $DIR_HASTA
rm -f !"($DIR_HASTA/*.csv)"
for i in $(ls $DIR_HASTA/*.csv); do
	if [ "$(wc -l $i | awk {'print $1'})" -le "1" ]; then
		echo "  borrando archivo con una fila o menos (vacíos)" $i
		rm "$i"
	fi
done
# Se pasan los nombres a minúsculas 
for i in $(ls $DIR_HASTA/*.csv | grep [A-Z]); do
	mv -i $i `echo $i | tr 'A-Z' 'a-z'`
done
# Se les agrega prefijo
for i in $(ls $DIR_HASTA/*.csv); do
	NOMBRE_ARCHIVO=$PREFIJO"_"$(basename -s .csv $i)".csv"
	NOMBRE_ARCHIVO=$(echo "$NOMBRE_ARCHIVO" | sed 's/-/_/g')
	mv $i $DIR_HASTA/$NOMBRE_ARCHIVO 
done