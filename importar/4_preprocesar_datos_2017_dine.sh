if [ "$1" = "2017-dine-generales" ]; then
	DIR=$2"_"$1
	for i in $(ls $DIR/*.csv); do 
 	    #echo "elimino punto de números..." $i
		sed -i 's/\.//g' $i
	done

    #echo "reemplazo guión en votos por cero..." $DIR/e17_dine_generales_2017definitivodndistrito.csv
	sed -i 's/; -/0/g' $DIR/e17_dine_generales_2017definitivodndistrito.csv
    #echo "reemplazo guión en votos por cero..." $DIR/e17_dine_generales_2017definitivosndistrito.csv
	sed -i 's/;   -  /0/g' $DIR/e17_dine_generales_2017definitivosndistrito.csv
    #echo "reemplazo guión en votos por cero..." $DIR/e17_dine_generales_2017definitivodndistritodn.csv
	sed -i 's/; -/0/g' $DIR/e17_dine_generales_2017definitivodndistritodn.csv
    #echo "agrego ; que falta en MOVIMIENTO NORTE GRANDE0..." $DIR/e17_dine*.csv
	sed -i 's/MOVIMIENTO NORTE GRANDE0/MOVIMIENTO NORTE GRANDE;0/g' $DIR/e17_dine_*.csv
fi
if [ "$1" = "2017-dine-paso" ]; then
	DIR=$2"_"$1
	for i in $(ls $DIR/*.csv); do 
 	    #echo "elimino espacios laterales..." $i
		tr -s " " < $i > $i.temp
		# eliminamos espacios antes y después
		sed -i -e 's/; /;/g' -e 's/ ;/;/g' $i.temp
		mv -f $i.temp $i
	done

    #echo "cambio coma por punto y coma..." $DIR/p17_dine_definitivopaso2017dntotaldistrito.csv
	sed -i 's/,/;/g' $DIR/p17_dine_definitivopaso2017dntotaldistrito.csv
    #echo "cambio coma por punto y coma..." $DIR/p17_dine_paso_2017definitivosdndistrito.csv
	sed -i 's/,/;/g' $DIR/p17_dine_paso_2017definitivosdndistrito.csv
    #echo "elimino punto de números..." $DIR/p17_dine_paso_2017definitivosndistrito.csv
	sed -i 's/\.//g' $DIR/p17_dine_paso_2017definitivosndistrito.csv

    #echo "agrego cero en votos que viene vacío..." $DIR/p17_dine_*.csv
	sed -i 's/CAPITAL FEDERAL;VOTOS RECURRIDOS E IMPUGNADOS;;/CAPITAL FEDERAL;VOTOS RECURRIDOS E IMPUGNADOS;;0/g' $DIR/p17_dine_*.csv
	sed -i 's/BUENOS AIRES;VOTOS RECURRIDOS E IMPUGNADOS;;/BUENOS AIRES;VOTOS RECURRIDOS E IMPUGNADOS;;0/g' $DIR/p17_dine_*.csv
	sed -i 's/NEUQUEN;VOTOS RECURRIDOS E IMPUGNADOS;;/NEUQUEN;VOTOS RECURRIDOS E IMPUGNADOS;;0/g' $DIR/p17_dine_*.csv
fi