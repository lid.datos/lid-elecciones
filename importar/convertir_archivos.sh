DESDE="ISO-8859-15"
HASTA="UTF-8"
for i in $(ls $1/*.csv); do
   iconv -f $DESDE -t $HASTA $i > $i.tmp
   mv $i.tmp $i
done