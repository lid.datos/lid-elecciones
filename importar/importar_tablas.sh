# Se exportan e importan tablas geográficas de distritos, secciones y circuitos de la base de 2019 a 2017
# Previamente es necesario instalar también postgis en la base de 2017 y eliminar las tablas de 2017 si existieran
echo "copiando tabla" $1 "a base" $2
sudo -u postgres -H -- pg_dump -t $1 postgres > $1.sql
sudo -u postgres -H -- psql -d $2 -f $1.sql
rm $1.sql