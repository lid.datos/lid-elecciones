formateo_columnas_lugares() {
	# Partiendo de la segunda fila, formateo distrito %02i, codigo local %05i, circu %5s, secc %03i
	# circu: como es alfanumerico, le elimino espacios, y luego se los agrego a izquierda: queda con espacios
	awk -F ";" 'NR>=2 { gsub(/ /,"",$9); printf  \
	"%02i;%s;%05i;%s;%s;%s;%s;%s;%5s;%03i; %s; %s; %s; %s; %s; %s\n" \
	 , $1,$2,  $3,$4,$5,$6,$7,$8, $9, $10,$11,$12,$13,$14,$15,$16}' $1 >> $1".tmp"

	# Luego a circu le reemplazo los espacios por ceros
	awk -F ";" '{ gsub(/ /,"0",$9); \
	print $1";"$2";"$3";"$4";"$5";"$6";"$7";"$8";"$9";"$10";"$11";"$12";"$13";"$14";"$15";"$16}' $1".tmp" > $1".tmp2"

	# Unifico nuevo header con cuerpo y borro temporales
	echo "distrito;provincia;codigo_local;desc_seccion;desc_circuito;localidad;descripcion;direccion;circuito;seccion;electores_lv;elec_agregdos;elec_tachados;elec_actuales;lista_mesas;cant_mesas_con_asoc" > $1
	cat  $1".tmp2" >> $1
	rm $1".tmp" $1".tmp2"
}

dividir_archivo_lugares() {
	# Escribe nuevo header con nuevo nombre de columna
	head -n 1 $1 | sed "s/lista_mesas/mesa/" > $1.tmp
	# Lee línea a línea desde la segunda fila
	tail -n +2 $1 | while read line; do
		FILA=$line
		# Toma la primera parte (en la que reemplazo espacios por ceros para circuitos) y la segunda parte de cada fila
		FILA1=$(echo $FILA | awk -F ";" '{print $1";"$2";"$3}')
		FILA2=$(echo $FILA | awk -F ";" '{print $5";"$6";"$7";"$8}')
		
		# Divide la lista de mesas en una fila por mesa con la mesa correspondiente
		MESAS_FILAS=$(echo $FILA | awk -F ";" '{print $4}' | tr "," "\n")
		for MESA in $MESAS_FILAS; do
			# A la mesa la formateo: primero espacios a la izquierda y luego los reeplazo por cero
			MESA=$(printf "%6s\n" $MESA | sed "s/ /0/g")
			echo $FILA1";"$MESA";"$FILA2 >> $1.tmp
		done
	done
	# Elimina archivo temporal
	mv -f $1.tmp $1
}

eliminar_duplicados() {
	head -n 1 $1 > $1".head"
	tail -n +2 $1 | sort | uniq > $1".tmp"
	cat $1".head" > $1
	cat $1".tmp" >> $1
	rm $1".head" $1".tmp"
}

extraer_votos_mesa() {
for i in $(ls $DIR_HASTA/e21_??_*.csv); do

	echo "extrayendo votos por mesa" $i
	# convierto archivos a formato unix
	dos2unix -q $i
	# reemplazo filas que terminen con una o dos comas
	sed -i -e 's/[\,\,]$//g' -e 's/[\,]$//' -e 's/\"//g' $i

	# PBA / Resto de distritos
	# 1   Agrupacion
	# 2   Cargo
	# 3   Codigo
	# 4   Distrito
	# 5   Establecimiento
	# 6   Fecha
	# 7   IdCargo
	# 8   IdCircuito
	# 9   IdDistrito
	# 10  SeccionProvincial   --
	# 11  IdSeccion           10
	# 12  Mesa                11
	# 13  Seccion             12
	# 14  idAgrupacion        13
	# 15  idAgrupacionInt     14
	# 16  idLista             15
	# 17  lista               16
	# 18  tipoVoto            17
	# 19  votos               18

	# Extraemos columnas votos_mesas y otras tablas partiendo de la segunda fila, sin encabezado
	if [[ $i == *"p21_02"* ]]; then

		# votos: formateo distrito %02i, codigo local %05i, circuito %5s, secc %03i
		# Primero elimino espacios y agrego espacios a izquierda
		awk -F ";" 'NR>=2 {gsub(/ /,"",$12); gsub(/ /,"",$7); gsub(/ /,"",$14); gsub(/ /,"",$15); gsub(/ /,"",$16); gsub(/ /,"",$18); gsub(/ /,"",$19); printf \
		"%02i;%03i;%s;%s;%6s;%2s;%4s;%4s;%4s;%10s;%s\n" \
		,$9  ,$11 ,$8,$3,$12 ,$7 ,$14,$15,$16,$18 ,$19}' $i > $i".tmp"
		# Reemplazo espacios por ceros
		awk -F ";" '{gsub(/ /,"0",$5); gsub(/ /,"0",$6); gsub(/ /,"0",$7); gsub(/ /,"0",$8); gsub(/ /,"0",$9); \
		print $1";"$2";"$3";"$4";"$5";"$6";"$7";"$8";"$9";"$10";"$11}' $i".tmp" >> $i".tmp2"
		# 1-distrito;2-seccion;3-circuito;4-codigo_local;5-mesa;6-categoria;7-agrupacion;8-agrupacionint;9-lista;10-tipo_voto;11-votos

		# secciones: 9-IdDistrito, 11-IdSeccion, 13-Seccion, 10-SeccionProvincial (sólo existe en PBA)
		awk -F ";" 'NR>=2 {printf "%02i;%03i;%s;%s\n",$9,$11,$13,$10}' $i >> $SECCIONES".tmp2"
		# 1-distrito;2-seccion;3-desc_seccion;4-seccion_provincial

		# circuitos: 9-IdDistrito, 11-IdSeccion, 8-IdCircuito
		awk -F ";" 'NR>=2 {printf "%02i;%03i;%s\n",$9,$11,$8}' $i >> $CIRCUITOS".tmp2"
		# 1-distrito;2-seccion;3-circuito

		# lugares: 9-IdDistrito, 11-IdSeccion, 8-IdCircuito, 3-Codigo, 5-Establecimiento
		awk -F ";" 'NR>=2 {printf "%02i;%03i;%s;%s;%s\n",$9,$11,$8,$3,$5}' $i >> $LUGARES".tmp2"
		# 1-distrito;2-seccion;3-circuito,4-codigo_local,5-desc_local

		# mesas  : 9-IdDistrito, 11-IdSeccion, 8-IdCircuito, 3-Codigo, 12-Mesa
		awk -F ";" 'NR>=2 {printf "%02i;%03i;%s;%s;%s\n",$9,$11,$8,$3,$12}' $i >> $MESAS".tmp2"
		# 1-distrito;2-seccion;3-circuito;4-codigo_local;5-mesa
	else
		# Partiendo de la segunda fila, formateo distrito %02i, codigo local %05i, circu %5s, secc %03i
		# circu: como es alfanumerico, le elimino espacios, y luego se los agrego a izquierda: queda con espacios
		awk -F ";" 'NR>=2 {gsub(/ /,"",$11); gsub(/ /,"",$7); gsub(/ /,"",$13); gsub(/ /,"",$14); gsub(/ /,"",$15); gsub(/ /,"",$17); gsub(/ /,"",$18); printf \
		"%02i;%03i;%s;%s;%6s;%2s;%4s;%4s;%4s;%10s;%s\n" \
		,$9  ,$10 ,$8,$3,$11 ,$7 ,$13,$14,$15,$17 ,$18}' $i > $i".tmp"
		# Luego a circu le reemplazo los espacios por ceros
		awk -F ";" '{gsub(/ /,"0",$5); gsub(/ /,"0",$6); gsub(/ /,"0",$7); gsub(/ /,"0",$8); gsub(/ /,"0",$9); \
		print $1";"$2";"$3";"$4";"$5";"$6";"$7";"$8";"$9";"$10";"$11}' $i".tmp" >> $i".tmp2"
		# 1-distrito;2-seccion;3-circuito;4-codigo_local;5-mesa;6-categoria;7-agrupacion;8-agrupacionint;9-lista;10-tipo_voto;11-votos

		# secciones: 9-IdDistrito, 10-IdSeccion, 12-Seccion, XX-Sección provincial (no existe en el resto)
		awk -F ";" 'NR>=2 {printf "%02i;%03i;%s;\n",$9,$10,$12}' $i >> $SECCIONES".tmp2"
		# 1-distrito;2-seccion;3-desc_seccion;4-seccion_provincial (vacío)

		# circuitos: 9-IdDistrito, 10-IdSeccion, 8-IdCircuito
		awk -F ";" 'NR>=2 {printf "%02i;%03i;%s\n",$9,$10,$8}' $i >> $CIRCUITOS".tmp2"
		# 1-distrito;2-seccion;3-circuito

		# lugares: 9-IdDistrito, 10-IdSeccion, 8-IdCircuito, 3-Codigo, 5-Establecimiento
		awk -F ";" 'NR>=2 {printf "%02i;%03i;%s;%s;%s\n",$9,$10,$8,$3,$5}' $i >> $LUGARES".tmp2"
		# 1-distrito;2-seccion;3-circuito;4-codigo_local;5-desc_local

		# mesas  : 9-IdDistrito, 10-IdSeccion, 8-IdCircuito, 3-Codigo, 11-Mesa
		awk -F ";" 'NR>=2 {printf "%02i;%03i;%s;%s;%s\n",$9,$10,$8,$3,$11}' $i >> $MESAS".tmp2"
		# 1-distrito;2-seccion;3-circuito;4-codigo_local;5-mesa
	fi

	# distritos: 9-IdDistrito, 4-Distrito
	awk -F ";" 'NR>=2 {printf "%02i;%s\n",$9,$4}' $i > $DISTRITOS".tmp"
	# Luego a circu le reemplazo los espacios por ceros
	awk -F ";" '{gsub(/ /,"0",$1); print $1";"$2}' $DISTRITOS".tmp" >> $DISTRITOS".tmp2"
	# 1-distrito;2-desc_distrito

done
}

unificar_votos_mesa() {

	echo "unificando en" $1

	# agrego nuevo head
	echo "distrito;seccion;circuito;codigo_local;mesa;categoria;agrupacion;agrupacionint;lista;tipo_voto;votos" > $1
	# recorro archivo temporales con las columnas extraídas
	for i in $(ls $DIR_HASTA/p21_??_*.tmp2); do
		tail -n +2 $i >> $1
	done

	echo "distrito;desc_distrito" > $DISTRITOS
	cat $DISTRITOS".tmp2" >> $DISTRITOS
	eliminar_duplicados $DISTRITOS
	# Corrijo espacio en distrito Tierra del Fuego
	sed -i 's/Fuego,Ant/Fuego, Ant/' $DISTRITOS

	echo "distrito;seccion;desc_seccion;seccion_provincial" > $SECCIONES
	cat $SECCIONES".tmp2" >> $SECCIONES
	eliminar_duplicados $SECCIONES

	echo "distrito;seccion;circuito" > $CIRCUITOS
	cat $CIRCUITOS".tmp2" >> $CIRCUITOS
	eliminar_duplicados $CIRCUITOS

	echo "distrito;seccion;circuito;codigo_local;desc_local" > $LUGARES
	cat $LUGARES".tmp2" >> $LUGARES
	eliminar_duplicados $LUGARES

	echo "distrito;seccion;circuito;codigo_local;mesa" > $MESAS
	cat $MESAS".tmp2" >> $MESAS
	eliminar_duplicados $MESAS

	# borro temporales y archivos por provincia
	rm $DIR_HASTA/*.tmp $DIR_HASTA/*.tmp2 $DIR_HASTA/p21_??_*.csv
}

# DIR_DESDE: No debe tener subdirectorios y ni nombres de archivos con caracteres especiales como paréntesis o espacios
DIR_DESDE=$1
PREFIJO=$2
DIR_HASTA=$PREFIJO"_"$DIR_DESDE

echo "  "$(date)"- Genero distritos, secciones y circuitos de lugares" 

MESAS_ESCRUTADAS=$DIR_HASTA/$PREFIJO"_mesas_escrutadas_cierre.csv"
MESAS_DISTRITOS=$DIR_HASTA/$PREFIJO"_"mesas_distritos.csv
awk -F ";" '{print $9";"$4}' $MESAS_ESCRUTADAS > $MESAS_DISTRITOS
eliminar_duplicados $MESAS_DISTRITOS

MESAS_ESCRUTADAS=$DIR_HASTA/$PREFIJO"_mesas_escrutadas_cierre.csv"
MESAS_SECCIONES=$DIR_HASTA/$PREFIJO"_"mesas_secciones.csv
awk -F ";" '{print $9";"$10}' $MESAS_ESCRUTADAS > $MESAS_SECCIONES
eliminar_duplicados $MESAS_SECCIONES

MESAS_ESCRUTADAS=$DIR_HASTA/$PREFIJO"_mesas_escrutadas_cierre.csv"
MESAS_CIRCUITOS=$DIR_HASTA/$PREFIJO"_"mesas_circuitos.csv
awk -F ";" '{print $9";"$10";"$8}' $MESAS_ESCRUTADAS > $MESAS_CIRCUITOS
eliminar_duplicados $MESAS_CIRCUITOS
