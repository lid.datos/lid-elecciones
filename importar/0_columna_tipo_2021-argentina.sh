# Determina el tipo de dato en la base de acuerdo al nombre de la columna 
case $1 in
*desc_seccion|*desc_circuito|*desc_distrito|*desc_local) 
	echo $1 "VARCHAR(100)" ;;
*denominacion*) 
	echo $1 "VARCHAR(100)" ;;
*seccion_provincial*) 
	echo $1 "VARCHAR(20)" ;;
*distrito*|*categoria)
	echo $1 "VARCHAR(2) NOT NULL" ;;
*agrupacion|*agrupacionint)
	echo $1 "VARCHAR(4) NOT NULL" ;;
*seccion)
	echo $1 "VARCHAR(3) NOT NULL" ;;
*municipio)
	echo $1 "VARCHAR(3) NOT NULL" ;;
*lista) 
	echo $1 "VARCHAR(4)" ;;
*lista__) 
	echo $1 "VARCHAR(4) NOT NULL" ;;
*circuito) 
	echo $1 "VARCHAR(5) NOT NULL" ;;
*codigo_local) 
	echo $1 "VARCHAR(5) NOT NULL" ;;
*mesa) 
	echo $1 "VARCHAR(6) NOT NULL" ;;
*tipo_voto) 
	echo $1 "VARCHAR(10) NOT NULL" ;;
*porc_*|*votos_*|*votos|*electores)
	echo $1 "INTEGER NOT NULL" ;;
*mesa|*agrupacion_politica) 
	echo $1 "VARCHAR(4) NOT NULL" ;;
*seccion*|*circuito*|*localidad*|*descripcion*|*direccion*) 
	echo $1 "VARCHAR(100)" ;;
*elec_actuales*)
	echo $1 "VARCHAR(20)" ;;
*electores_lv*|*elec_agregdos*|*elec_tachados*|*cant_mesas_con_asoc*|*mes|*dia|*hora|*minuto)
	echo $1 "SMALLINT NOT NULL" ;;
*)
	echo $1 "VARCHAR" ;;
esac