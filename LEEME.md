LID Elecciones 
==============

Documento de presentación del Proyecto:
https://docs.google.com/document/d/1mOVXZwBSNG7M42i69rpuB2Fx1ZBj7Qf2-6D4PmaRn6k

Drive del proyecto:
https://drive.google.com/drive/folders/1jXBvLFTzb2NiXBfXZFlU20ZpvG4entUh

Directorios de trabajo, en cada uno de ellos un archivo LEEME.rst con mayor detalle si lo requiere:

descargar
---------

  Sripts de descargas de archivos, scrapeos y telegramas descargados

importar
--------

Datasets y sripts de importación de los mismos a tablas de una base de datos PostGreSQL

exportar
--------

  Sripts y consultas SQL para la extracción de datos de una base de datos.
  Y su posterior procesamiento para su uso en Flourish u otras formas de visualización

procesar_telegramas
-------------------

  Proyecto para extracción de metadatos de imágenes de los Telegramas electorales en Python.
  En desarollo, partimos de un ejemplo de uso y tenemos guardados telegramas de algunos procesos electorales.

