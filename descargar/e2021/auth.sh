USUARIO=$(grep usuario .config | cut -d'=' -f2)
PASS=$(grep pass .config | cut -d'=' -f2)
curl --location --request POST 'https://cognito-idp.us-east-1.amazonaws.com/' \
--header 'X-Amz-Target: AWSCognitoIdentityProviderService.InitiateAuth' \
--header 'Content-Type: application/x-amz-json-1.1' \
--data-raw '{
    "AuthParameters": {
        "USERNAME": "$USUARIO",
        "PASSWORD": "$PASS"
    },
    "AuthFlow": "USER_PASSWORD_AUTH",
    "ClientId": "f0dapcjldcsbslvnnqhtvngjs",
    "UserPoolId": "us-east-1_xp4TpXSkw"
}'