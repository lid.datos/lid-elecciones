DIR=e2021-json
DIR_SALIDA=salidas-json
ACCESO=$(grep acceso .config | cut -d'=' -f2)
HOST=$(grep simulador .config | cut -d'=' -f2)
DIR_DESTINO=$(grep dir_simula .config | cut -d'=' -f2)
#while [ true ]
#do
  echo $(date)" -------- Inciando descarga y generación de provincias (aprox 1 minuto) -----------" 
  # Descargo otras provincias
  ./descargar.sh 1
  # Genero html
  ./json2csv.sh 0
  # Genero provincias
  ./json2csv.sh 1
  echo $(date)" -------- GENERACIÓN FINALIZADA !!!" 

  # Descargo otras categorías
  ./descargar.sh 0
  # Copio provincias para simuladores
  cp -f $DIR/01.json $DIR/02.json $DIR/10.json $DIR_SALIDA
  echo $(date)" -------- Copiando provincias a servidor simuladores -----------" 
  scp -i $ACCESO $DIR_SALIDA/* $HOST:$DIR_DESTINO
  #sleep 200 
  echo $(date)" -------- Esperando 4 mintos para nueva descarga -----------" 
#done