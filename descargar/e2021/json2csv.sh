function clasifica_fuerza() {
	FUERZA=$(echo $1|tr -d '"')
	case $FUERZA in
	  "JUNTOS"|"JUNTOS POR EL CAMBIO"|"CHACO CAMBIA + JUNTOS POR EL CAMBIO"|"JUNTOS POR EL CAMBIO CHUBUT"|"JUNTOS POR ENTRE RÍOS"|"JUNTOS POR FORMOSA LIBRE"|"CAMBIA JUJUY"|"CAMBIA MENDOZA"|"FRENTE JUNTOS POR EL CAMBIO"|"CAMBIA NEUQUÉN"|"JUNTOS POR EL CAMBIO JXC"|"CAMBIA SANTA CRUZ"|"JUNTOS POR EL CAMBIO TIERRA DEL FUEGO"|"JUNTOS POR EL CAMBIO +"|"CAMBIA SANTA CRUZ"|"VAMOS LA RIOJA"|"ECO + VAMOS CORRIENTES"|"UNIDOS POR SAN LUIS")
		return 1 ;;
	  "FRENTE DE TODOS"|"FRENTE DE TODOS - TODOS SAN JUAN"|"FRENTE CIVICO POR SANTIAGO"|"TODOS UNIDOS")
		return 2 ;;
	  "FRENTE DE IZQUIERDA Y DE TRABAJADORES - UNIDAD"|"DEL OBRERO"|"NUEVA IZQUIERDA"|"POR UN SANTIAGO OBRERO"|"OBRERO")
		return 3 ;;
	  "LA LIBERTAD AVANZA"|"AVANZA LIBERTAD"|"LIBERTAD, VALORES Y CAMBIO"|"UNIÓN POPULAR FEDERAL"|"LIBERTAD, VALORES Y CAMBIO"|"REPUBLICANOS UNIDOS"|"UNIÓN CÍVICA RADICAL")
		return 4 ;;
	*)  return 0 ;;
	esac
}

function color_fuerza() {
	case $1 in
	  "JUNTOS"|"JUNTOS POR EL CAMBIO"|"CHACO CAMBIA + JUNTOS POR EL CAMBIO"|"JUNTOS POR EL CAMBIO CHUBUT"|"JUNTOS POR ENTRE RÍOS"|"JUNTOS POR FORMOSA LIBRE"|"CAMBIA JUJUY"|"CAMBIA MENDOZA"|"FRENTE JUNTOS POR EL CAMBIO"|"CAMBIA NEUQUÉN"|"JUNTOS POR EL CAMBIO JXC"|"CAMBIA SANTA CRUZ"|"JUNTOS POR EL CAMBIO TIERRA DEL FUEGO"|"JUNTOS POR EL CAMBIO +"|"VAMOS LA RIOJA"|"ECO + VAMOS CORRIENTES"|"UNIDOS POR SAN LUIS")
		COLOR="#EDAD08" ;;
	  "FRENTE DE TODOS"|"FRENTE DE TODOS - TODOS SAN JUAN"|"FRENTE CIVICO POR SANTIAGO"|"TODOS UNIDOS"|"FRENTE RENOVADOR")
		COLOR="#1D6996" ;;
	  "FRENTE DE IZQUIERDA Y DE TRABAJADORES - UNIDAD"|"DEL OBRERO"|"NUEVA IZQUIERDA"|"POR UN SANTIAGO OBRERO"|"OBRERO"|"NUEVA IZQUIERDA"|"NUEVA IZQUIERDA")
		COLOR="#B22222" ;;
	  "LA LIBERTAD AVANZA"|"AVANZA LIBERTAD"|"LIBERTAD, VALORES Y CAMBIO"|"UNIÓN POPULAR FEDERAL"|"LIBERTAD, VALORES Y CAMBIO"|"REPUBLICANOS UNIDOS"|"UNION DEL CENTRO DEMOCRATICO"|"COMPROMISO FEDERAL"|"UNIÓN CÍVICA RADICAL")
		COLOR="#5C3D2E" ;;
	  "AUTODETERMINACIÓN Y LIBERTAD"|"FRENTE VAMOS CON VOS"|"FRENTE AMPLIO CATAMARQUEÑO"|"HACEMOS POR CÓRDOBA"|"INDEPENDIENTE DEL CHUBUT"|"SOCIALISTA"|"MOVIMIENTO AL SOCIALISMO"|"UNIDOS"|"VERDE"|"FEDERAL"|"COALICIÓN CÍVICA - AFIRMACIÓN PARA UNA REPÚBLICA IGUALITARIA (ARI)"|"MOVIMIENTO LIBRES DEL SUR"|"FRENTE SI + PRS"|"FUERZA SAN LUIS"|"SOMOS ENERGÍA PARA RENOVAR SANTA CRUZ -SER-"|"FRENTE AMPLIO PROGRESISTA"|"PODEMOS"|"MOVIMIENTO LIBRES DEL SUR"|"FRENTE AMPLIO POR TUCUMÁN"|"MOVIMIENTO POPULAR FUEGUINO"|"VAMOS CON VOS")
		COLOR="#3F0071" ;;
	  "+ VALORES"|"JUNTOS"|"CORRIENTE DE PENSAMIENTO BONAERENSE"|"FRENTE INTEGRADOR"|"CHUBUT SOMOS TODOS"|"CONSERVADOR POPULAR"|"FE"|"PRINCIPIOS Y CONVICCIÓN"|"VAMOS ! MENDOCINOS"|"FRENTE RENOVADOR DE LA CONCORDIA"|"MOVIMIENTO POPULAR NEUQUINO"|"JUNTOS SOMOS RIO NEGRO"|"UNIDOS POR SALTA"|"CONSENSO ISCHIGUALASTO"|"PRIMERO SANTA FE"|"SOBERANIA POPULAR"|"UNITE POR LA LIBERTAD Y LA DIGNIDAD"|"FRENTE PATRIOTICO LABORISTA"|"UNITE POR LA LIBERTAD Y LA DIGNIDAD"|"FUERZA REPUBLICANA"|"SOMOS FUEGUINOS")
		COLOR="#00A19D" ;;
	*)  COLOR="#000000" ;;
	esac
}

function generar_html_inicio() {
	ARCH_HASTA=$1
	INICIO="<table>"
	echo $INICIO >> $ARCH_HASTA
}

function generar_html_fin() {
	ARCH_HASTA=$1

	FILA="<tr style='color:#000000'><td style='font-weight:900'>En blanco</td><td style='font-weight:900;text-align:end'>{{Votos en blanco}}</td><td style='font-weight:900;text-align:end'>{{% Votos en blanco}}%</td></tr>"
	echo $FILA >> $ARCH_HASTA
	FILA="<tr style='color:#000000'><td style='font-weight:900'>Nulos</td><td style='font-weight:900;text-align:end'>{{Votos nulos}}</td><td style='font-weight:900;text-align:end'>{{% Votos nulos}}%</td></tr>"
	echo $FILA >> $ARCH_HASTA
	FILA="<tr style='color:#000000'><td style='font-weight:900'>Votos R/C/I</td><td style='font-weight:900;text-align:end'>{{Votos R/C/I}}</td><td style='font-weight:900;text-align:end'>{{% Votos R/C/I}}%</td></tr>"
	echo $FILA >> $ARCH_HASTA
	FILA="</table><hr><table>"

	echo $FILA >> $ARCH_HASTA
	FILA="<tr><td>Participación</td><td style='text-align:end'>{{% Participación}}% ({{Cant. Votantes}} de {{Cant. Electores}})</td></tr>"
	echo $FILA >> $ARCH_HASTA
	FILA="<tr><td>Mesas totalizadas</td><td style='text-align:end'>{{% Mesas Totalizadas}}% ({{Mesas Totalizadas}} de {{Mesas Esperadas}})</td></tr>"
	echo $FILA >> $ARCH_HASTA
	FILA="<tr><td>Actualización</td><td style='text-align:end'>{{Fecha Totalización}}</td></tr>"
	echo $FILA >> $ARCH_HASTA
	FILA="</table>"
	echo -n $FILA >> $ARCH_HASTA
}

function generar_html() {
	ARCH_DESDE=$1
	ARCH_HASTA=$2
	DISTRITO=$3
	COLOR="#000000"

	PARTE2="}}'><td style='font-weight:900'>"
	PARTE3="</td><td style='font-weight:900;text-align:end'>{{"
	PARTE4="}}</td><td style='font-weight:900;text-align:end'>{{% "
	PARTE5="}}%</td></tr>"
	# Recorro agrupaciones
	CANT=$(cat $ARCH_DESDE | jq '.valoresTotalizadosPositivos' | jq length)
	for (( j=0; j<$CANT ; j++ )); do
		# Recorro de cero a cant agrupaciones y extraigo cada valor
		VOTOS_POSITIVOS=".valoresTotalizadosPositivos[$j]"
		NOMBRE=$(cat $ARCH_DESDE | jq $VOTOS_POSITIVOS | jq -r '[.nombreAgrupacion] | @csv' |sed 's/\"//g' |tr '\n' ' '|xargs)
		color_fuerza "$NOMBRE" $j
		PARTE1="<tr style='color:"$COLOR";display:{{"
		echo $PARTE1$DISTRITO"-"$NOMBRE$PARTE2$NOMBRE$PARTE3$DISTRITO"-"$NOMBRE$PARTE4$DISTRITO"-"$NOMBRE$PARTE5 >> $ARCH_HASTA
	done
}

function generar_csv_distrito() {
	ARCH_DESDE=$1
	ARCH_HASTA=$2

	# Info general
	echo '"Fecha Totalización"',$(cat $ARCH_DESDE | jq '.fechaTotalizacion') >> $ARCH_HASTA
	echo '"Mesas Esperadas"',$(cat $ARCH_DESDE | jq '.estadoRecuento' | jq -r '[.mesasEsperadas] | @csv') >> $ARCH_HASTA
	echo '"Mesas Totalizadas"',$(cat $ARCH_DESDE | jq '.estadoRecuento' | jq -r '[.mesasTotalizadas,.mesasTotalizadasPorcentaje] | @csv') >> $ARCH_HASTA
	echo '"Cantidad Electores"',$(cat $ARCH_DESDE | jq '.estadoRecuento' | jq -r '[.cantidadElectores] | @csv') >> $ARCH_HASTA
	echo '"Cantidad Votantes"',$(cat $ARCH_DESDE | jq '.estadoRecuento' | jq -r '[.cantidadVotantes] | @csv') >> $ARCH_HASTA
	echo '"Participación Porcentaje"',$(cat $ARCH_DESDE | jq '.estadoRecuento' | jq -r '[.participacionPorcentaje] | @csv') >> $ARCH_HASTA

	# Recorro agrupaciones
	CANT=$(cat $ARCH_DESDE | jq '.valoresTotalizadosPositivos' | jq length)
	for (( j=0; j<$CANT ; j++ )); do
		# Recorro de cero a cant agrupaciones y extraigo cada valor
		VOTOS_POSITIVOS=".valoresTotalizadosPositivos[$j]"
		cat $ARCH_DESDE | jq $VOTOS_POSITIVOS | jq -r '[.nombreAgrupacion,.votos,.votosPorcentaje] | @csv' >> $ARCH_HASTA
	done

	# Votos no positivos
	echo '"VOTOS BLANCOS"',$(cat $ARCH_DESDE | jq '.valoresTotalizadosOtros' | jq -r '[.votosEnBlanco,.votosEnBlancoPorcentaje] | @csv') >> $ARCH_HASTA
	echo '"VOTOS NULOS"',$(cat $ARCH_DESDE | jq '.valoresTotalizadosOtros' | jq -r '[.votosNulos,.votosNulosPorcentaje] | @csv') >> $ARCH_HASTA
	echo '"VOTOS R/C/I"',$(cat $ARCH_DESDE | jq '.valoresTotalizadosOtros' | jq -r '[.votosRecurridosComandoImpugnados,.votosRecurridosComandoImpugnadosPorcentaje] | @csv') >> $ARCH_HASTA
}	

function primeras_columnas() {
	ARCH_DESDE=$1
	ARCH_HASTA=$2
	DISTRITO=$3
	SECCION=$4
	MESA_CIRCUITO=$5

	echo >> $ARCH_HASTA
	# Info general
	echo -n "$DISTRITO" >> $ARCH_HASTA
	if [[ "$SECCION" != "" ]]; then
		echo -n ","$DISTRITO$SECCION >> $ARCH_HASTA
		echo -n ","$SECCION >> $ARCH_HASTA
	fi
	if [[ "$MESA_CIRCUITO" != "" ]]; then
		echo -n ","$DISTRITO$SECCION"0"$MESA_CIRCUITO >> $ARCH_HASTA
		echo -n ","$MESA_CIRCUITO >> $ARCH_HASTA
	fi
	echo -n ","$(cat $ARCH_DESDE | jq '.fechaTotalizacion') >> $ARCH_HASTA
	echo -n ","$(cat $ARCH_DESDE | jq '.estadoRecuento' | jq -r '[.mesasEsperadas] | @csv') >> $ARCH_HASTA
	echo -n ","$(cat $ARCH_DESDE | jq '.estadoRecuento' | jq -r '[.mesasTotalizadas] | @csv') >> $ARCH_HASTA
	echo -n ","$(cat $ARCH_DESDE | jq '.estadoRecuento' | jq -r '[.mesasTotalizadasPorcentaje] | @csv') >> $ARCH_HASTA
	echo -n ","$(cat $ARCH_DESDE | jq '.estadoRecuento' | jq -r '[.cantidadElectores] | @csv') >> $ARCH_HASTA
	echo -n ","$(cat $ARCH_DESDE | jq '.estadoRecuento' | jq -r '[.cantidadVotantes] | @csv') >> $ARCH_HASTA
	echo -n ","$(cat $ARCH_DESDE | jq '.estadoRecuento' | jq -r '[.participacionPorcentaje] | @csv') >> $ARCH_HASTA
	# Votos no positivos
	echo -n ","$(cat $ARCH_DESDE | jq '.valoresTotalizadosOtros' | jq -r '[.votosEnBlanco] | @csv') >> $ARCH_HASTA
	echo -n ","$(cat $ARCH_DESDE | jq '.valoresTotalizadosOtros' | jq -r '[.votosEnBlancoPorcentaje] | @csv') >> $ARCH_HASTA
	echo -n ","$(cat $ARCH_DESDE | jq '.valoresTotalizadosOtros' | jq -r '[.votosNulos] | @csv') >> $ARCH_HASTA
	echo -n ","$(cat $ARCH_DESDE | jq '.valoresTotalizadosOtros' | jq -r '[.votosNulosPorcentaje] | @csv') >> $ARCH_HASTA
	echo -n ","$(cat $ARCH_DESDE | jq '.valoresTotalizadosOtros' | jq -r '[.votosRecurridosComandoImpugnados] | @csv') >> $ARCH_HASTA
	echo -n ","$(cat $ARCH_DESDE | jq '.valoresTotalizadosOtros' | jq -r '[.votosRecurridosComandoImpugnadosPorcentaje] | @csv') >> $ARCH_HASTA
}

function generar_html_csv() {

	echo "Generando html y csv" $(date)
	ARCH_HTML=$DIR_HASTA"/"$HORA"_popup.html"
	ARCH_CSV=$DIR_HASTA"/"$HORA"_resumen_distritos.csv"
	rm -f $ARCH_HTML $ARCH_CSV

	generar_html_inicio $ARCH_HTML
	for i in $(ls $DIR_DESDE/??.json); do
		DISTRITO=$(basename -s .json $i)
		ARCH_DESDE=$DIR_DESDE/$DISTRITO.json
		# generar_csv_distrito $ARCH_DESDE $ARCH_CSV
		generar_html $ARCH_DESDE $ARCH_HTML $DISTRITO
	done
	generar_html_fin $ARCH_HTML
}

function generar_total_pais_fila() {

	for (( j=0; j<$CANT ; j++ )); do
		# Recorro de cero a cant agrupaciones y extraigo cada valor
		VOTOS_POSITIVOS=".valoresTotalizadosPositivos[$j]"
		VOTOS=$(cat $ARCH_DESDE | jq $VOTOS_POSITIVOS | jq -r '[.votos] | @csv' | tr -d '\n')
		VOTOS_PORC=$(cat $ARCH_DESDE | jq $VOTOS_POSITIVOS | jq -r '[.votosPorcentaje] | @csv' | tr -d '\n')
		FUERZA=$(cat $ARCH_DESDE | jq $VOTOS_POSITIVOS | jq -r '[.nombreAgrupacion] | @csv' | tr -d '\n')

		if [[ $VOTOS -ge $MAX ]]; then GANADOR=$FUERZA; MAX=$VOTOS;	fi
		# echo $FUERZA $VOTOS $GANADOR $MAX

		clasifica_fuerza "$FUERZA"
		case $? in 
		1) JUNTOS=$VOTOS
		   JUNTOS_PORC=$VOTOS_PORC ;;
		2) FDT=$VOTOS
		   FDT_PORC=$VOTOS_PORC ;;
		3) FITU=$VOTOS
		   FITU_PORC=$VOTOS_PORC ;;
		4) LIBER=$VOTOS
		   LIBER_PORC=$VOTOS_PORC ;;
		0) OTROS=$(awk "BEGIN {print $OTROS + $VOTOS; exit}") 
		   OTROS_PORC=$(awk "BEGIN {print $OTROS_PORC + $VOTOS_PORC; exit}") ;;
		esac
	done
	echo -n ","$JUNTOS","$JUNTOS_PORC","$FDT","$FDT_PORC","$FITU","$FITU_PORC","$LIBER","$LIBER_PORC","$OTROS","$OTROS_PORC","$GANADOR >> $ARCH_HASTA

	# Recorro agrupaciones acumuladas
	# echo "Antes  :" $COL 
	for (( j=0; j<$COL ; j++ )); do
		echo -n ",none" >> $ARCH_HASTA
	done

	DISTRITO=$1
	ARCH_DISTRITO="$DIR_DESDE/$DISTRITO.json"
	for (( j=0; j<$CANT ; j++ )); do
		# Recorro de cero a cant agrupaciones y extraigo cada valor
		VOTOS_POSITIVOS=".valoresTotalizadosPositivos[$j]"
		if [[ "$CANT_SECCIONES" == "0" ]]; then
			# echo "Leyendo header de archivo distrito:" $ARCH_DISTRITO
			REEMPLAZAR="s/\\\"/\\\"$DISTRITO-/"
			echo -n "," >> $HEADER
			cat $ARCH_DISTRITO | jq $VOTOS_POSITIVOS | jq -r '[.nombreAgrupacion] | @csv' | tr -d '\n' | sed $REEMPLAZAR >> $HEADER
			echo -n "," >> $HEADER
			cat $ARCH_DISTRITO | jq $VOTOS_POSITIVOS | jq -r '[.nombreAgrupacion] | @csv' | tr -d '\n' | sed $REEMPLAZAR | sed 's/\"/\"% /' >> $HEADER
		fi
		NOMBRE=$(cat $ARCH_DISTRITO | jq $VOTOS_POSITIVOS | jq -r '[.nombreAgrupacion] | @csv' | tr -d '\n')
		echo -n "," >> $ARCH_HASTA
		cat $ARCH_DESDE | jq '.valoresTotalizadosPositivos[] | select(.nombreAgrupacion=='"$NOMBRE"') | .votos' | tr -d '\n' >> $ARCH_HASTA
		echo -n "," >> $ARCH_HASTA
		cat $ARCH_DESDE | jq '.valoresTotalizadosPositivos[] | select(.nombreAgrupacion=='"$NOMBRE"') | .votosPorcentaje' | tr -d '\n' >> $ARCH_HASTA
	done

	COL_HASTA=$COL+$CANT+$CANT
	# echo "Después:" $COL_HASTA
	for (( j=$COL_HASTA; j<$FIN ; j++ )); do
		echo -n ",none" >> $ARCH_HASTA
	done
	CANT_SECCIONES=$CANT_SECCIONES+1
}

function generar_total_pais() {

	ARCH_DESDE=$1
	HEADER=$2
	ARCH_HASTA=$3
	DISTRITO=$4
	SECCION=$5
	CIRCUITO=$6

	echo $ARCH_DESDE "-->" $ARCH_HASTA
	primeras_columnas $ARCH_DESDE $ARCH_HASTA $DISTRITO $SECCION $CIRCUITO

	# Acumular y totalizar
	MAX=0; GANADOR=""; JUNTOS=0; FDT=0; FITU=0; LIBER=0; OTROS=0
	VOTOS_PORC=0.0; JUNTOS_PORC=0.0; FDT_PORC=0.0; FITU_PORC=0.0; LIBER_PORC=0.0; OTROS_PORC=0.0

	if [[ "$DISTRITO" != "$DISTRITO_ANTERIOR" ]]; then
		# echo "Cambia distrito" $DISTRITO "->" $DISTRITO_ANTERIOR
		DISTRITO_ANTERIOR=$DISTRITO
		COL=$COL+$CANT+$CANT
		CANT_SECCIONES=0
	fi

	# Recorro agrupaciones
	CANT=$(cat $ARCH_DESDE | jq '.valoresTotalizadosPositivos' | jq length)
	# echo "Agrupaciones: "$CANT

	generar_total_pais_fila $DISTRITO
}	

function generar_total_distritos() {

	echo "Generando total país distritos (10 seg aprox)"

	HEADER=$DIR_HASTA"/header_total_pais.csv"
	HEADER_FIJO='"Distrito","Fecha Totalización","Mesas Esperadas","Mesas Totalizadas","% Mesas Totalizadas","Cant. Electores","Cant. Votantes","% Participación","Votos en blanco","% Votos en blanco","Votos nulos","% Votos nulos","Votos R/C/I","% Votos R/C/I","JC","% JC","FDT","% FDT","FITU","% FITU","LIBER","% LIBER","OTROS","% OTROS","Ganador"'
	echo -n $HEADER_FIJO > $HEADER

	TOTAL_PAIS=$DIR_HASTA"/"$HORA"_total_pais.csv"
	TMP=$DIR_HASTA"/tmp_total_pais.csv"

	COL=0
	FIN=268 # Cantidad de agrupaciones x 2 (votos y porcentaje)
	DISTRITO_ANTERIOR="01"
	CANT_SECCIONES=0;

	for i in $(ls $DIR_DESDE/??.json); do
		DISTRITO=$(basename -s .json $i)
		generar_total_pais $i $HEADER $TMP $DISTRITO
	done

	cat $HEADER $TMP > $TOTAL_PAIS
	rm -f $TMP $HEADER
}	

function generar_total_secciones() {

	echo "Generando total país x secciones (5 min aprox)"

	HEADER=$DIR_HASTA"/header_total_pais_seccion.csv"
	HEADER_FIJO='"Distrito","Cod Sección","Sección","Fecha Totalización","Mesas Esperadas","Mesas Totalizadas","% Mesas Totalizadas","Cant. Electores","Cant. Votantes","% Participación","Votos en blanco","% Votos en blanco","Votos nulos","% Votos nulos","Votos R/C/I","% Votos R/C/I","JC","% JC","FDT","% FDT","FITU","% FITU","LIBER","% LIBER","OTROS","% OTROS","Ganador"'
	echo -n $HEADER_FIJO > $HEADER

	TOTAL_PAIS_SECCIONES=$DIR_HASTA"/"$HORA"_total_pais_secciones.csv"
	TMP=$DIR_HASTA"/tmp_total_pais_seccion.csv"

	COL=0
	FIN=268 # Cantidad de agrupaciones x 2 (votos y porcentaje)
	DISTRITO_ANTERIOR="01"
	CANT_SECCIONES=0;

	for i in $(ls $DIR_DESDE/??_???.json); do
		NOMBRE=$(basename -s .json $i)
		DISTRITO=$(echo $NOMBRE | awk -F"_" '{print $1}')
		SECCION=$(echo $NOMBRE | awk -F"_" '{print $2}')
		generar_total_pais $i $HEADER $TMP $DISTRITO $SECCION
	done

	cat $HEADER $TMP > $TOTAL_PAIS_SECCIONES
	rm -f $TMP $HEADER
}	

function generar_total_circuitos() {

	echo "Generando total país x circuitos (XX min aprox)"

	HEADER=$DIR_HASTA"/header_total_pais_circuito.csv"
	HEADER_FIJO='"Distrito","Cod Sección","Sección","Cod Circuito","Circuito","Fecha Totalización","Mesas Esperadas","Mesas Totalizadas","% Mesas Totalizadas","Cant. Electores","Cant. Votantes","% Participación","Votos en blanco","% Votos en blanco","Votos nulos","% Votos nulos","Votos R/C/I","% Votos R/C/I","JC","% JC","FDT","% FDT","FITU","% FITU","LIBER","% LIBER","OTROS","% OTROS","Ganador"'
	echo -n $HEADER_FIJO > $HEADER

	TOTAL_PAIS_CIRCUITOS=$DIR_HASTA"/"$HORA"_total_pais_circuitos.csv"
	TMP=$DIR_HASTA"/tmp_total_pais_circuito.csv"

	COL=0
	FIN=268 # Cantidad de agrupaciones x 2 (votos y porcentaje)
	DISTRITO_ANTERIOR="01"
	CANT_SECCIONES=0;

	for i in $(ls $DIR_DESDE/??_???_*.json); do
		NOMBRE=$(basename -s .json $i)
		DISTRITO=$(echo $NOMBRE | awk -F"_" '{print $1}')
		SECCION=$(echo $NOMBRE | awk -F"_" '{print $2}')
		CIRCUITO=$(echo $NOMBRE | awk -F"_" '{print $3}')
		generar_total_pais $i $HEADER $TMP $DISTRITO $SECCION $CIRCUITO
	done

	cat $HEADER $TMP > $TOTAL_PAIS_CIRCUITOS
	rm -f $TMP $HEADER
}	

function generar_fila_csv() {
 	ARCH_DESDE=$1
 	ARCH_HASTA=$2
 	DISTRITO=$3
 	SECCION=$4
 	MESA=$5

 	echo $ARCH_DESDE $ARCH_HASTA
	echo "Generando csv mesas" $DISTRITO $SECCION $MESA
	echo -n "$DISTRITO" >> $ARCH_HASTA
	echo -n ","$DISTRITO$SECCION >> $ARCH_HASTA
	echo -n ","$SECCION >> $ARCH_HASTA
	echo -n ","$MESA >> $ARCH_HASTA

	# Info general
	echo -n ","$(cat $ARCH_DESDE | jq '.fechaTotalizacion') >> $ARCH_HASTA
	echo -n ","$(cat $ARCH_DESDE | jq '.estadoRecuento' | jq -r '[.mesasEsperadas] | @csv') >> $ARCH_HASTA
	echo -n ","$(cat $ARCH_DESDE | jq '.estadoRecuento' | jq -r '[.mesasTotalizadas] | @csv') >> $ARCH_HASTA
	echo -n ","$(cat $ARCH_DESDE | jq '.estadoRecuento' | jq -r '[.mesasTotalizadasPorcentaje] | @csv') >> $ARCH_HASTA
	echo -n ","$(cat $ARCH_DESDE | jq '.estadoRecuento' | jq -r '[.cantidadElectores] | @csv') >> $ARCH_HASTA
	echo -n ","$(cat $ARCH_DESDE | jq '.estadoRecuento' | jq -r '[.cantidadVotantes] | @csv') >> $ARCH_HASTA
	echo -n ","$(cat $ARCH_DESDE | jq '.estadoRecuento' | jq -r '[.participacionPorcentaje] | @csv') >> $ARCH_HASTA

	ARCH_DISTRITO=e2021-json/02.json
	# Recorro agrupaciones
	CANT=$(cat $ARCH_DESDE | jq '.valoresTotalizadosPositivos' | jq length)
	for (( j=0; j<$CANT ; j++ )); do
		# Recorro de cero a cant agrupaciones y extraigo cada valor
		VOTOS_POSITIVOS=".valoresTotalizadosPositivos[$j]"
		NOMBRE=$(cat $ARCH_DISTRITO | jq $VOTOS_POSITIVOS | jq -r '[.nombreAgrupacion] | @csv' | tr -d '\n')
		echo -n "," >> $ARCH_HASTA
		cat $ARCH_DESDE | jq '.valoresTotalizadosPositivos[] | select(.nombreAgrupacion=='"$NOMBRE"') | .votos' | tr -d '\n' >> $ARCH_HASTA
		echo -n "," >> $ARCH_HASTA
		cat $ARCH_DESDE | jq '.valoresTotalizadosPositivos[] | select(.nombreAgrupacion=='"$NOMBRE"') | .votosPorcentaje' | tr -d '\n' >> $ARCH_HASTA
	done

	# Votos no positivos
	echo -n ","$(cat $ARCH_DESDE | jq '.valoresTotalizadosOtros' | jq -r '[.votosEnBlanco] | @csv') >> $ARCH_HASTA
	echo -n ","$(cat $ARCH_DESDE | jq '.valoresTotalizadosOtros' | jq -r '[.votosEnBlancoPorcentaje] | @csv') >> $ARCH_HASTA
	echo -n ","$(cat $ARCH_DESDE | jq '.valoresTotalizadosOtros' | jq -r '[.votosNulos] | @csv') >> $ARCH_HASTA
	echo -n ","$(cat $ARCH_DESDE | jq '.valoresTotalizadosOtros' | jq -r '[.votosNulosPorcentaje] | @csv') >> $ARCH_HASTA
	echo -n ","$(cat $ARCH_DESDE | jq '.valoresTotalizadosOtros' | jq -r '[.votosRecurridosComandoImpugnados] | @csv') >> $ARCH_HASTA
	echo -n ","$(cat $ARCH_DESDE | jq '.valoresTotalizadosOtros' | jq -r '[.votosRecurridosComandoImpugnadosPorcentaje] | @csv') >> $ARCH_HASTA
	echo >> $ARCH_HASTA
}	

function generar_total_mesas() {

	DISTRITO=$1
	echo "Generando csv mesas para" $DISTRITO

	ARCH_MESAS=$DIR_HASTA"/mesas_"$DISTRITO".csv"
	HEADER='"Distrito","Cod Sección","Sección","Mesa","Fecha Totalización","Mesas Esperadas","Mesas Totalizadas","% Mesas Totalizadas","Cant. Electores","Cant. Votantes","% Participación","JUNTOS","% JUNTOS","FRENTE DE TODOS","% FRENTE DE TODOS","AVANZA LIBERTAD","% AVANZA LIBERTAD","FRENTE DE IZQUIERDA Y DE TRABAJADORES - UNIDAD","% FRENTE DE IZQUIERDA Y DE TRABAJADORES - UNIDAD","FRENTE VAMOS CON VOS","% FRENTE VAMOS CON VOS","+ VALORES","% + VALORES","Votos en blanco","% Votos en blanco","Votos nulos","% Votos nulos","Votos R/C/I","% Votos R/C/I"'
	echo $HEADER > $ARCH_MESAS

	for i in $(ls "$DIR_DESDE/$DISTRITO"_???_*".json"); do
		NOMBRE=$(basename -s .json $i)
		DISTRITO=$(echo $NOMBRE | awk -F"_" '{print $1}')
		SECCION=$(echo $NOMBRE | awk -F"_" '{print $2}')
		MESA=$(echo $NOMBRE | awk -F"_" '{print $3}')
		if [ -s $i ]; then # Si no está vacío
		  generar_fila_csv $i $ARCH_MESAS $DISTRITO $SECCION $MESA
		fi		
	done
}	

: '
json2csv.sh [0|1|2]
0 html y csv       (5 segundos)
1 total distritos (10 segundos)
2 total secciones (aprox 5 min)
3 total circuitos 
4 total mesas     (Jujuy 1684)
'

DIR_DESDE="e2021-json"
DIR_HASTA="e2021-csv"
HORA=$(date +'%e_%H%M')

echo "Inicio" $(date)
case $1 in
  0) generar_html_csv ;;
  1) generar_total_distritos ;;
  2) generar_total_secciones ;;
  3) generar_total_circuitos ;;
  4) generar_total_mesas $2  ;;
  *) echo "json2csv.sh [0|1|2|3|4] (0 html, 1 distritos, 2 secciones, 3 circuitos, 4 mesas)" ;;
esac
echo "Fin   " $(date)