
# JQ Tutorial: https://stedolan.github.io/jq/tutorial/
# Convertir json a csv 
# http://www.compciv.org/recipes/cli/jq-for-parsing-json/
# https://shapeshed.com/jq-json/
# https://lzone.de/cheat-sheet/jq
# JSON Viewer https://jsonformatter.org/json-viewer

# Ejemplos distrito
cat 02.json | jq '.'
cat 02.json | jq '.valoresTotalizadosOtros'
cat 02.json | jq '.estadoRecuento'
cat 02.json | jq '.valoresTotalizadosPositivos'
cat 02.json | jq '.valoresTotalizadosPositivos[0]'
cat 02.json | jq '.valoresTotalizadosPositivos[0] .nombreAgrupacion'
cat 02.json | jq '.valoresTotalizadosPositivos[0] .votos'
cat 02.json | jq '.valoresTotalizadosPositivos[0]' | jq '.nombreAgrupacion,.votos,.votosPorcentaje'
cat 02.json | jq '.valoresTotalizadosPositivos[0]' | jq -r '[.nombreAgrupacion,.votos,.votosPorcentaje]'
cat 02.json | jq '.valoresTotalizadosPositivos[0]' | jq -r '[.nombreAgrupacion,.votos,.votosPorcentaje] | @csv'
cat 02.json | jq '.valoresTotalizadosPositivos[0][]'
cat 02.json | jq '.valoresTotalizadosOtros'
cat 02.json | jq '.valoresTotalizadosOtros[]'
cat 02.json | jq -r '[.valoresTotalizadosOtros[]]'
cat 02.json | jq -r '[.valoresTotalizadosOtros[]] | @csv'

: '
Comentarios
'
#URL="$HOST/estados/estadoRecuento?distritoId=03&categoriaId=03"
#URL="$HOST/estados/estadoRecuento?categoriaId=3&distritoId=02&seccionProvincialId=2&seccionId=010&municpioId=010&circuitoId=00117&establecimientoId=04216&mesaId=0201000010X"
#URL="$HOST/resultados/getTiff?mesaId=0100100008X"
#URL="$HOST/resultados/csv/getTotalesListas/02"
#URL="$HOST/resultados/getResultados?categoriaId=3&distritoId=02&seccionProvincialId=2&seccionId=010&municpioId=010&circuitoId=00117&establecimientoId=04216&mesaId=0201000011X"

# Totales Secciones CABA en csv
#URL="$HOST/resultados/csv/getTotales/01" 

# Resultados mesa en json: Buenos Aires - Arrecife - 000011
#URL="$HOST/resultados/getResultados?categoriaId=3&distritoId=02&seccionProvincialId=2&seccionId=010&municpioId=010&circuitoId=00117&establecimientoId=04216&mesaId=0201000011X"

# Resultados escuela en json: Buenos Aires - Arrecife - ESCUELA ES N°2 --> FALTAN MESAS
#URL="$HOST/resultados/getResultados?categoriaId=3&distritoId=02&seccionProvincialId=2&seccionId=010&municpioId=010&circuitoId=00117&establecimientoId=04216"

# Resultados circuito: Buenos Aires - Arrecife - 00117
#URL="$HOST/resultados/getResultados?categoriaId=3&distritoId=02&seccionProvincialId=2&seccionId=010&municpioId=010&circuitoId=00117"

#URL="$HOST/catalogo/csv/getAmbitos"
#URL="$HOST/catalogo/catalogo/getAgrupaciones?categoriaId=3&distritoId=02&seccionProvincialId=2&seccionId=010&municpioId=010&circuitoId=00117&establecimientoId=04216&mesaId=0201000010X
