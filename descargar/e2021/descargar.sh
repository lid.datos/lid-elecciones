# API http://api.resultados.gob.ar/
# Resultados para flourish https://drive.google.com/drive/u/0/folders/1WkGWwl1OWfhbHs8flHgKTxTbEWT9faP0
HOST=$(grep host .config | cut -d'=' -f2)
TOKEN=$(grep token .config | cut -d'=' -f2)
DIR="e2021-json"
DIR_CATALOGO="catalogo"
DIR_SALIDAS_JSON="salidas-json"

# Categorías
# Diputados nacionales  : 3
# Senadores nacionales  : 2
# Senadores provinciales: 5
# Diputados provinciales: 6
# Consejales :  7
# Intendentes: 10

function descargar_distritos() {
  echo "Descargando distrito" $1
	DISTRITO=$1
	# Resultados por provincia
	sleep 0.5
	URL="$HOST/resultados/getResultados?categoriaId=$CATEGORIA&distritoId=$DISTRITO"
	curl --header "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" "$URL" -o "$DIR/$DISTRITO.json"
}

function descargar_secciones() {
  echo "Descargando secciones" $1
	DISTRITO=$1
	# Resultados por sección
	grep "^"$1"." $DIR_CATALOGO/p21_secciones.csv | while read line ; do
    SECCION=$(echo $line | awk -F";" '{print $2}')
		URL="$HOST/resultados/getResultados?categoriaId=$CATEGORIA&distritoId=$DISTRITO&seccionId=$SECCION"
		sleep 0.5
		curl --header "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" "$URL" -o $DIR/$DISTRITO"_"$SECCION".json"
	done
}

function descargar_circuitos() {
  echo "Descargando circuitos" $1
	DISTRITO=$1
	# Resultados por sección
	grep "^"$1"." $DIR_CATALOGO/e21_circuitos.csv | while read line ; do
    SECCION=$(echo $line | awk -F";" '{print $2}')
    CIRCUITO=$(echo $line | awk -F";" '{print $3}')
		URL="$HOST/resultados/getResultados?categoriaId=$CATEGORIA&distritoId=$DISTRITO&seccionId=$SECCION&circuitoId=$CIRCUITO"
		sleep 0.1
		curl --header "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" "$URL" -o $DIR/$DISTRITO"_"$SECCION"_"$CIRCUITO".json"
	done
}

function descargar_mesas() {
	DISTRITO=$1
	# Resultados por mesa
	grep "^"$1"." $DIR_CATALOGO/p21_mesas.csv | while read line ; do
    SECCION=$(echo $line | awk -F";" '{print $2}')
    MESA=$(echo $line | awk -F";" '{print $5}')
    echo $DISTRITO $SECCION $MESA
		URL="$HOST/resultados/getResultados?categoriaId=$CATEGORIA&distritoId=$DISTRITO&seccionId=$SECCION&mesaId=$DISTRITO$SECCION$MESA"
		#URL="$HOST/resultados/getResultados?categoriaId=3&distritoId=10&seccionId=010&mesaId=0201000011X"	
		sleep 0.3
		curl --header "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" "$URL" -o $DIR/$DISTRITO"_"$SECCION"_"$MESA".json"
	done
}

function descargar_cat_distrito() {
	sleep 0.5
	URL="$HOST/resultados/getResultados?categoriaId=$1&distritoId=$2"
	curl --header "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" "$URL" -o "$DIR_SALIDAS_JSON/$2_cat$1.json"
}

function descargar_cat_sec_prov() {
	sleep 0.5
	URL="$HOST/resultados/getResultados?categoriaId=$1&distritoId=$2&seccionProvincialId=$3"
	curl --header "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" "$URL" -o "$DIR_SALIDAS_JSON/$2_cat$1_$3sec.json"
}

function descargar_cat_seccion() {
	sleep 0.5
	URL="$HOST/resultados/getResultados?categoriaId=$1&distritoId=$2&seccionId=$3"
	curl --header "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" "$URL" -o "$DIR_SALIDAS_JSON/$2_$3_cat$1.json"
}

function descargar_otras_categorias() {
  echo "Descargando otras categorías..."

  # Diputados Provinciales
  descargar_cat_distrito "6" "01"
  descargar_cat_sec_prov "6" "02" "3"
  descargar_cat_sec_prov "6" "13" "1"

  # Consejales PBA
	grep "^02." $DIR_CATALOGO/p21_secciones.csv | while read line ; do
    SECCION=$(echo $line | awk -F";" '{print $2}')
    descargar_cat_seccion "7" "02" $SECCION
	done
}

function descargar() {
	case $2 in
	  1) descargar_distritos $1 ;; # 30 segundos
	  2) descargar_secciones $1 ;; # 15 minutos
	  3) descargar_circuitos $1 ;; # Estimado 2hs aprox
	  4) descargar_mesas $1     ;; # Jujuy 1684 mesas - 28 min
	esac
}

echo "Inicio" $(date)
CATEGORIA="3"

case $1 in
0) descargar_otras_categorias ;;
1|2|3)
	 cat $DIR_CATALOGO/p21_distritos.csv | while read line ; do
	   DISTRITO=$(echo $line | awk -F";" '{print $1}')
	   descargar $DISTRITO $1
	 done ;;
4) descargar_mesas $2 $1 ;;
*) echo "descargar.sh [1|2|3|4] [DISTRITO - optativo] (0 otras categorías, 1 distritos, 2 secciones, 3 circuitos, 4 mesas)" 
esac
echo "Fin" $(date)
