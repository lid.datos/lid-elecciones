function descarga() {
	# Categorías:
	# Diputados nacionales: 3
	# Senadores nacionales: 2
	# Senadores provinciales: 5
	# Diputados provinciales: 6
	# Consejales: 7
	# Intendentes: 10
############################################################################################################
	wget -nc -c https://resultados.gob.ar/backend-difu-arg/scope/data/getTiff/$1X -O "descargas/$1X.tiff.json"
	# wget -nc -c https://resultados.gob.ar/backend-difu-arg/scope/data/getScopeData/$1X/3 -O "descargas/$1X-3.json"
	# wget -nc -c https://resultados.gob.ar/backend-difu-arg/scope/data/getScopeData/$1X/2 -O "descargas/$1X-2.json"
	# wget -nc -c https://resultados.gob.ar/backend-difu-arg/scope/data/getScopeData/$1X/5 -O "descargas/$1X-5.json"
	# wget -nc -c https://resultados.gob.ar/backend-difu-arg/scope/data/getScopeData/$1X/6 -O "descargas/$1X-6.json"
}

while read line; do
	descarga $line
done < mesas.csv
