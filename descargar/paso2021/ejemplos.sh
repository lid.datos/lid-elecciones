# Ejemplos de descargas con wget

# API Elección Corrientes 2021
# CSV Rtdos mesas
https://apielecciones.corrientes.gob.ar/api/file/csv/generate?mesa_id=1
# Telegramas mesas
https://apielecciones.corrientes.gob.ar/telegramas/1.jpg
# PDF informes mesas
https://apielecciones.corrientes.gob.ar/api/file/pdf/generate?mesa_id=1
para pdf

# Primero armar archivo plano con los datos a descargar en filas, por ejemplo
# codigo_mesa,nombre_archivo
# 1,mesa_id_1_escrutinio_310821_1008SJR.csv
# 2,mesa_id_2_escrutinio_310821_1008SJR.csv
# ...
# Después sustituir de manera que nos quede un archivo .sh, y cambiarle permisos para hacerlo ejecutable

# Ejemplos de descargas con wget descarga con un nombre de archivo determinado
wget https://apielecciones.corrientes.gob.ar/api/file/csv/generate?mesa_id=1 -O "mesa_id_1_escrutinio_310821_1008SJR.csv"
wget https://apielecciones.corrientes.gob.ar/api/file/pdf/generate?mesa_id=1 -O "mesa_id_1_escrutinio_310821_1008SJR.pdf"
wget https://apielecciones.corrientes.gob.ar/telegramas/1.jpg -O "mesa_id_1_escrutinio_310821_1008SJR.pdf"

# Opción -c: Permite retomar la descarga si se corta, salteando ya descargados
wget -c https://apielecciones.corrientes.gob.ar/api/file/csv/generate?mesa_id=1 -O "mesa_id_1_escrutinio_310821_1008SJR.csv"
wget -c https://apielecciones.corrientes.gob.ar/api/file/pdf/generate?mesa_id=1 -O "mesa_id_1_escrutinio_310821_1008SJR.pdf"
wget -c https://apielecciones.corrientes.gob.ar/telegramas/1.jpg -O "mesa_id_1_escrutinio_310821_1008SJR.pdf"



# Ejemplos de consultas PASO 2021

https://resultados.gob.ar/elecciones/3/25315/1/-1/-1/Ciudad-Aut%C3%B3noma-de-Buenos-Aires/Comuna-3/00030/Esc.-N%C2%BA8-Dr.-Arturo-Mateo-Bas/0100300929X#agrupaciones
https://resultados.gob.ar/backend-difu-arg/scope/data/getScopeData/0100300929X/3
https://resultados.gob.ar/backend-difu-arg/scope/data/getScopeData/0100300929X/6
https://resultados.gob.ar/backend-difu-arg/scope/data/getTiff/0100300929X

https://resultados.gob.ar/elecciones/3/49302/1/-1/-1/Buenos-Aires/Secci%C3%B3n-Tercera/La-Matanza/La-Matanza/0635F/ESCUELA-EP-N%C2%B033%2FES-N%C2%B059/0206102927X
https://resultados.gob.ar/backend-difu-arg/scope/data/getScopeData/0206102927X/3
https://resultados.gob.ar/backend-difu-arg/scope/data/getScopeData/0206102927X/6
https://resultados.gob.ar/backend-difu-arg/scope/data/getScopeData/0206102927X/7
https://resultados.gob.ar/backend-difu-arg/scope/data/getTiff/0206102927X
