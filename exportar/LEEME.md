Exportar
========

En el repositorio mantenemos solamente los scripts.
Los scripts generan consultas de resultados y las ejecutan generando los csv correspondientes.
Pendiente de procesarlos para calcular porcentajes y ganadores.

1 - p2021-resultados
--------------------
Contiene los resultados electorales generados a partir de las consultas a la base de datos
https://drive.google.com/drive/folders/1o49Nfblqh6wZTWRjK8F8Xnbzh-7MOSur
