SELECT t.categoria
     , CASE
        WHEN t.categoria = '02' THEN 'Senadores Nacionales'
        WHEN t.categoria = '03' THEN 'Diputados Nacionales'
        WHEN t.categoria = '05' THEN 'Senadores Provinciales'
        WHEN t.categoria = '06' THEN 'Diputados Provinciales'
        WHEN t.categoria = '07' THEN 'Consejales'
        WHEN t.categoria = '10' THEN 'Intendentes'
      END categoria
     , t.distrito || t.seccion || '0' || t.circuito cod_circuito
     , t.distrito, d.desc_distrito
     , s.seccion_provincial
     , t.seccion, s.desc_seccion || ' - Circuito: '|| t.circuito "desc_seccion", t.circuito
-- FILTROS
, SUM(t.votos) FILTER (WHERE t.agrupacion = '0502') AS "Juntos por el Cambio"
, SUM(t.votos) FILTER (WHERE t.agrupacion = '0501') AS "Frente de Todos"
, SUM(t.votos) FILTER (WHERE t.agrupacion = '0167') AS "Somos Fueguinos"
, SUM(t.votos) FILTER (WHERE t.agrupacion = '0212') AS "Repulicando Unidos"
, SUM(t.votos) FILTER (WHERE t.agrupacion = '0089') AS "Obrero"
, SUM(t.votos) FILTER (WHERE t.agrupacion = '0154') AS "Movimiento Popular Fueguino"
, SUM(t.votos) FILTER (WHERE t.agrupacion = '0214') AS "Movimiento al Socialismo"
SELECT t.categoria
     , CASE
        WHEN t.categoria = '02' THEN 'Senadores Nacionales'
        WHEN t.categoria = '03' THEN 'Diputados Nacionales'
        WHEN t.categoria = '05' THEN 'Senadores Provinciales'
        WHEN t.categoria = '06' THEN 'Diputados Provinciales'
        WHEN t.categoria = '07' THEN 'Consejales'
        WHEN t.categoria = '10' THEN 'Intendentes'
      END categoria
     , t.distrito || t.seccion || '0' || t.circuito cod_circuito
     , t.distrito, d.desc_distrito
     , s.seccion_provincial
     , t.seccion, s.desc_seccion || ' - Circuito: '|| t.circuito "desc_seccion", t.circuito
-- FILTROS
     , SUM(t.votos) FILTER (WHERE t.agrupacion = '0000' AND TRIM(t.tipo_voto) = 'blancos') AS "Blancos"
     , SUM(t.votos) FILTER (WHERE t.agrupacion = '0000' AND TRIM(t.tipo_voto) = 'nulos') AS "Nulos"
     , SUM(t.votos) FILTER (WHERE t.agrupacion = '0000' AND TRIM(t.tipo_voto) NOT IN ('blancos','nulos')) AS "Recurridos, impugandos, comando"
     , COUNT(DISTINCT t.mesa) "Mesas"
     , SUM(t.votos) "Votos"
     , SUM(m.electores) FILTER (WHERE t.agrupacion = '0000' AND TRIM(t.tipo_voto) = 'blancos') AS "Electores"
FROM public.p21_votos t
   , public.p21_distritos d
   , public.p21_secciones s
   , public.p21_lugares l
   , public.p21_listas li
   , public.p21_electores_por_mesa m
WHERE t.distrito = d.distrito
AND t.distrito = s.distrito
AND t.seccion = s.seccion
AND t.distrito = l.distrito
AND t.seccion = l.seccion
