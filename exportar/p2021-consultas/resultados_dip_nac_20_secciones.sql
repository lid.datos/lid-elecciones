SELECT t.categoria
     , CASE
        WHEN t.categoria = '02' THEN 'Senadores Nacionales'
        WHEN t.categoria = '03' THEN 'Diputados Nacionales'
        WHEN t.categoria = '05' THEN 'Senadores Provinciales'
        WHEN t.categoria = '06' THEN 'Diputados Provinciales'
        WHEN t.categoria = '07' THEN 'Consejales'
        WHEN t.categoria = '10' THEN 'Intendentes'
      END categoria
     , t.distrito || t.seccion cod_seccion
     , t.distrito, d.desc_distrito
     , s.seccion_provincial
     , t.seccion, s.desc_seccion
-- FILTROS
, SUM(t.votos) FILTER (WHERE t.agrupacion = '0501') AS "Cambia Santa Cruz"
, SUM(t.votos) FILTER (WHERE t.agrupacion = '0502') AS "Frente de Todos"
, SUM(t.votos) FILTER (WHERE t.agrupacion = '0182') AS "Somos Energía para renovar Sta Cruz"
, SUM(t.votos) FILTER (WHERE t.agrupacion = '0503') AS "Frente de Izquierda y de los Trabajadores - Unidad"
, SUM(t.votos) FILTER (WHERE t.agrupacion = '0172') AS "Movimiento al Socialismo"
     , SUM(t.votos) FILTER (WHERE t.agrupacion = '0000' AND TRIM(t.tipo_voto) = 'blancos') AS "Blancos"
     , SUM(t.votos) FILTER (WHERE t.agrupacion = '0000' AND TRIM(t.tipo_voto) = 'nulos') AS "Nulos"
     , SUM(t.votos) FILTER (WHERE t.agrupacion = '0000' AND TRIM(t.tipo_voto) NOT IN ('blancos','nulos')) AS "Recurridos, impugandos, comando"
     , COUNT(DISTINCT t.mesa) "Mesas"
     , SUM(t.votos) "Votos"
     , SUM(m.electores) FILTER (WHERE t.agrupacion = '0000' AND TRIM(t.tipo_voto) = 'blancos') AS "Electores"
FROM public.p21_votos t
   , public.p21_distritos d
   , public.p21_secciones s
   , public.p21_lugares l
   , public.p21_listas li
   , public.p21_electores_por_mesa m
WHERE t.distrito = d.distrito
AND t.distrito = s.distrito
AND t.seccion = s.seccion
AND t.distrito = l.distrito
AND t.seccion = l.seccion
AND t.circuito = l.circuito
AND t.codigo_local = l.codigo_local
AND t.distrito = m.distrito
AND t.seccion = m.seccion
AND t.circuito = m.circuito
AND t.mesa = m.mesa
AND t.distrito = li.distrito
AND t.categoria = li.categoria
AND t.agrupacionint = li.agrupacion_politica 
AND t.lista = li.lista
AND t.categoria = '03'
AND t.distrito = '20'
GROUP BY t.categoria, t.distrito, d.desc_distrito, s.seccion_provincial, t.seccion, s.desc_seccion
ORDER BY t.distrito,t.seccion