# Directorio por defecto de PostgreSQL
DIR_PG=/var/lib/postgresql
BASE_DE_DATOS=elecciones2021

genera_consulta_distrito() {
	DIR=$2

	SALIDA="$DIR/resultados_dip_nac_$3_secciones.sql"
	head -n 14 "$1/resultados_secciones.sql" > $SALIDA
	cat "$1/$3_filtros.sql" >> $SALIDA
	tail -n 31 "$1/resultados_secciones.sql" >> $SALIDA
	sed -i '0,/?/s//03/' $SALIDA
	REEMPLAZAR="0,/?/s//$3/"
	sed -i $REEMPLAZAR $SALIDA
	sed -i 's/﻿//g' $SALIDA

	SALIDA="$DIR/resultados_dip_nac_$3_circuitos.sql"
	head -n 14 "$1/resultados_circuitos.sql" > $SALIDA
	cat "$1/$3_filtros.sql" >> $SALIDA
	head -n 31 "$1/resultados_circuitos.sql" >> $SALIDA
	sed -i '0,/?/s//03/' $SALIDA
	REEMPLAZAR="0,/?/s//$3/"
	sed -i $REEMPLAZAR $SALIDA
	sed -i 's/﻿//g' $SALIDA

	SALIDA="$DIR/resultados_dip_nac_$3_mesas.sql"
	head -n 14 "$1/resultados_mesas.sql" > $SALIDA
	cat "$1/$3_filtros.sql" >> $SALIDA
	tail -n 31 "$1/resultados_mesas.sql" >> $SALIDA
	sed -i '0,/?/s//03/' $SALIDA
	REEMPLAZAR="0,/?/s//$3/"
	sed -i $REEMPLAZAR $SALIDA
	sed -i 's/﻿//g' $SALIDA
}

1_generar_consultas() {
	echo "1_generar_consultas... $1 -> $2"
	genera_consulta_distrito $1 $2 01
	genera_consulta_distrito $1 $2 02
	genera_consulta_distrito $1 $2 03
	genera_consulta_distrito $1 $2 04
	genera_consulta_distrito $1 $2 05
	genera_consulta_distrito $1 $2 06
	genera_consulta_distrito $1 $2 07
	genera_consulta_distrito $1 $2 08
	genera_consulta_distrito $1 $2 09
	genera_consulta_distrito $1 $2 10
	genera_consulta_distrito $1 $2 11
	genera_consulta_distrito $1 $2 12
	genera_consulta_distrito $1 $2 13
	genera_consulta_distrito $1 $2 14
	genera_consulta_distrito $1 $2 15
	genera_consulta_distrito $1 $2 16
	genera_consulta_distrito $1 $2 17
	genera_consulta_distrito $1 $2 18
	genera_consulta_distrito $1 $2 19
	genera_consulta_distrito $1 $2 20
	genera_consulta_distrito $1 $2 21
	genera_consulta_distrito $1 $2 22
	genera_consulta_distrito $1 $2 23
	genera_consulta_distrito $1 $2 24
}

2_exportar_consultas() {
	DIR_DESDE=$1
	DIR_HASTA=$2

	date
	echo "2_exportar_consultas: exportando consultas sql a csv... $DIR_DESDE --> $DIR_HASTA (tiempo estimado puede llegar hasta aprox 4hs)"
	for i in $(sudo ls $DIR_DESDE/*.sql); do
		sudo cp $i $DIR_PG
		CONSULTA=$(basename -s .sql $i)
		echo "exportando $CONSULTA"
		sudo -u postgres -H -- psql -d elecciones2021 -A -F";" -f "$DIR_PG/$CONSULTA.sql" -o "$DIR_PG/$CONSULTA.csv" --pset footer
		sudo mv "$DIR_PG/$CONSULTA.csv" $DIR_HASTA
		sudo rm -f "$DIR_PG/$CONSULTA.sql"
	done
	date
}

1_generar_consultas p2021-sql p2021-consultas
2_exportar_consultas p2021-consultas p2021-resultados